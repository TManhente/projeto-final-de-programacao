project(CppWfms CXX)
cmake_minimum_required(VERSION 3.16)


# Habilita C++11 no GCC/Clang (MSVC já habilita por padrão, demais compiladores não são suportados ainda)
# ===========================
include(CheckCXXCompilerFlag)
check_cxx_compiler_flag(-std=c++11 HAS_STDCXX11_OPTION)
check_cxx_compiler_flag(-std=c++0x HAS_STDCXX0X_OPTION)
if(HAS_STDCXX11_OPTION)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(HAS_STDCXX0X_OPTION)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
endif()


# Define níveis de warning padrões do projeto
# ===========================================
set(CPPWFMS_DISABLE_WARNINGS FALSE CACHE BOOL "Desabilita a definição dos níveis de warning padrões do CPPWFMS.")
if(NOT CPPWFMS_DISABLE_WARNINGS)

	# Flags de warning do GCC/Clang
	set(GCC_CLANG_WARNING_OPTIONS "-Wall -Wextra -pedantic")
	check_cxx_compiler_flag(${GCC_CLANG_WARNING_OPTIONS} HAS_GCC_CLANG_WARNING_OPTIONS)

	set(MSVC_WARNING_OPTIONS "/W4")
	check_cxx_compiler_flag(${MSVC_WARNING_OPTIONS} HAS_MSVC_WARNING_OPTIONS)

	if(HAS_GCC_CLANG_WARNING_OPTIONS)
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_CLANG_WARNING_OPTIONS}")
	elseif(HAS_MSVC_WARNING_OPTIONS)
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MSVC_WARNING_OPTIONS}")
	else()
		message(WARN "Flags padrões de warning não puderam ser definidas.")
	endif()

endif()


add_subdirectory(docs)
add_subdirectory(petriNet)
add_subdirectory(src)


find_package(GTest)

if( GTEST_FOUND )
	enable_testing()
	add_subdirectory(tests)
else()
	message(WARN "GTest not found, tests targets won't be generated.")
endif()
