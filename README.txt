Pontifícia Universidade Católica do Rio de Janeiro
Departamento de Informática – Mestrado em Engenharia de Software
INF2102 – Projeto Final de Programação	 Professor: Carlos José P. de Lucena
Aluno: Thiago Manhente de C. Marques        Matrícula: 1221703        2013.2


             FRAMWORK DE SISTEMA DE GERENCIAMENTO DE WORKFLOWS CIENTÍFICOS EM C++
         ============================================================================

Sobre
-----

CPPWFMS é um framework para especificação e execução de workflows em C++.


Informações legais
------------------

Este software foi escrito entre 2013 e 2014 por Thiago Manhente de Carvalho Marques.

Exceto quando explicitamente indicado o contrário, e à extensão permita por lei, o autor dedica todo
direito autoral e direitos conexos para este software ao domínio público em todo o mundo.

Este software é distribuído sem garantias.

Uma cópia da Declaração de Domínio Público CC0 é provida no arquivo LEGAL.txt na raíz do projeto e
em http://creativecommons.org/publicdomain/zero/1.0/.


Pré-Requisitos
--------------

Para usar esta biblioteca, são necessários:

	* Um compilador C++ com bom suporte ao padrão C++11.
	  Em especial, é necessário suporte a variadic templates, std::bind, std::shared_ptr, std::tuple
	  e <type_traits>.
	  Durante o desenvolvimento, foram testados os compiladores Clang 3.2, GCC 4.8.2 e Microsoft
	  Visual C++ 12.

	* Boost
	  http://www.boost.org/.
	  São usadas as bibliotecas Graph, Iterator e Variant.

Para compilar os testes unitários, também são necessários:

	* CMake
	  Sistema de construção multi-plataforma e de código aberto.
	  http://www.cmake.org/

	* Google Test
	  Framework para escrita de testes em C++ para várias plataformas. Baseado na arquitetura xUnit.
	  http://code.google.com/p/googletest/


Instalação
----------

Este framework é basicamente uma biblioteca de cabeçalhos apenas (arquivos .hpp), e não precisa de
nenhuma etapa de construção específica para ser usado. Basta copiar todo o conteúdo do projeto para
uma pasta no seu sistema de arquivos e adicinoá-la aos caminhoa de "include" do compilador C++.


Testes
------

Para compilar os testes da biblioteca, deve-se usar o CMake. Além disso, é necessário que o CMake
encontre o Google Test, caso contrário o target `test` não será gerado.

O diretório raiz do projeto possui o arquivo CMakeLitst.txt, que é usado pelo CMake para gerar o
sistema de construção de acordo com a plataforma.

* Para geradores "Unix Makefiles" ou "NMake Makefiles", os seguintes comandos devem ser suficientes:

	cd .../CppWfms  # Diretório raíz do projeto
	mkdir build     # Diretório aonde serão criados os arquivos de construção e compilação
	cd build
	cmake ..        # Gera os arquivos makefile
	make            # (ou nmake), faz o build
	make test       # Executa os testes

  *Nota*
  Para geradores baseados em NMake ou Visual Studio, deve-se disparar o CMake de um terminal em que
  o compilador cl.exe e outras ferramentas do Visual C++ estejam no PATH. O Visual Studio disponibi-
  liza arquivos `vcvarsall.bat` que configuram essas variáveis, além de atalhos no menu iniciar para
  disparar terminais com essas variáveis configuradas.

  
* Para geradores de projetos de IDE, como "Visual Studio 12":

  Deve-se executar os mesmo 4 primeiros passos citados acime e, então, abrir o arquivo de projeto da
  IDE criado pelo CMake. Deve-se, então, selecionar o target `RUN_TESTS` na árvore do projeto dentro
  da IDE.


Também pode-se usar a interface gráfica `cmake-gui` para gerar os projetos.

Caso o CMake não consiga encontrar as bibliotecas necessárias nos diretórios padrões do sistema (ou
você queira utilizar outras versões delas), deve-se indicar para o CMake a sua localização através
das variáveis de ambiente ou cache adequadas:

	* Para Boost: `Boost_INCLUDE_DIRS`
	  http://www.cmake.org/cmake/help/v2.8.12/cmake.html#module:FindBoost
	* Para Google Test: `GTEST_ROOT`
	  http://www.cmake.org/cmake/help/v2.8.12/cmake.html#module:FindGTest

Essas variáveis de cache podem ser específicadas com a opção `-D<var>=<value` do comando `cmake`:

	cmake -DGTEST_ROOT=/opt/gtest-custom-install ..

Elas também podem ser definidas pelas interfaces gráficas `cmake-gui` e `ccmake`.


Executar os testes manualmente
------------------------------

Cada arquivo de teste na pasta `tests`, ao ser compilado, gera um executável com mesmo nome na pasta
`build\tests`. Basta disparar esses executáveis para rodar os testes.

Como esses executáveis são gerados usando o Google Test, tanto a sua saída quanto suas opções de
execução podem ser personalizadas. Use a opção `--help` nos executáveis de teste para mais
informações, e veja também a seção "Running Test Programs: Advanced Options" em
http://code.google.com/p/googletest/wiki/AdvancedGuide.

Os targets `test` e `RUN_TESTS` gerados pelo CMake são um atalho para invocar a execução de cada um
desses testes, além de exibir os resultados de execução de forma mais resumida. A saída de cada
teste rodado via esses targets é armazenada no diretório `Testing` da pasta de construção.


Documentação da API
-------------------

A documentação da API pode ser gerada a partir do código-fonte usando o Doxygen
(http://www.stack.nl/~dimitri/doxygen/).

O diretório `docs` contém um modelo de Doxyfile que é populado pelo CMake. Assim, para gerar a
documentação, rode

	cd build
	make doc

ou então rode o Doxygen diretamente a partir do Doxyfile gerado na pasta de construção:

	cd build
	doxygen Doxyfile

A documentação é gerada dentro da pasta `build\docs\html`.

Por padrão, é criada apenas a documentação da API pública. Para gerar a documentação completa,
incluindo estruturas internas e privadas, defina a variável DOC_INTERNAL_DOCS como ON no cache do
CMake.
