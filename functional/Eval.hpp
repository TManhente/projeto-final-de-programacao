#ifndef FUNCTIONAL_EVAL_HPP
#define FUNCTIONAL_EVAL_HPP


/**
 * @file Eval.hpp
 * Estruturas genéricas para invocação de funções e objetos-funções.
 * Retirado de http://functionalcpp.wordpress.com/2013/08/03/generalized-function-evaluation/
 */


#include <type_traits>
#include <utility>


namespace functional {


/// functions, functors, lambdas, etc.
template<
    class F, class... Args,
    class = typename std::enable_if<!std::is_member_function_pointer<F>::value>::type,
    class = typename std::enable_if<!std::is_member_object_pointer<F>::value>::type
    >
auto eval(F&& f, Args&&... args) -> decltype(f(std::forward<Args>(args)...))
{
    return f(std::forward<Args>(args)...);
}

///@name Especializações para funções-membro constantes
//@{

/// Especialização para funções-membro constante chamdas sobre objetos `c`
/// constantes
template<class R, class C, class... Args>
auto eval(R(C::*f)() const, const C& c, Args&&... args) -> R
{
    return (c.*f)(std::forward<Args>(args)...);
}

/// Especialização para funções-membro constante chamdas sobre objetos `c`
/// não-constantes
template<class R, class C, class... Args>
auto eval(R(C::*f)() const, C& c, Args&&... args) -> R
{
    return (c.*f)(std::forward<Args>(args)...);
}

//@}


/// Especialização para funções-membro não-constantes
template<class R, class C, class... Args>
auto eval(R(C::*f)(), C& c, Args&&... args) -> R
{
    return (c.*f)(std::forward<Args>(args)...);
}

///@name Especialiação para objetos-membro
//@{

/// Especialização para objetos-membro de objetos `c` constantes
template<class R, class C>
auto eval(R(C::*m), const C& c) -> const R&
{
    return c.*m;
}

/// Especialização para objetos-membro de objetos `c` não-constantes
template<class R, class C>
auto eval(R(C::*m), C& c) -> R&
{
    return c.*m;
}

//@}


}  // namespace functional


#endif  // FUNCTIONAL_EVAL_HPP
