#ifndef FUNCTIONAL_FUNCTIONTRAITS_HPP
#define FUNCTIONAL_FUNCTIONTRAITS_HPP


/**
 * @file FunctionTraits.hpp
 * Estrutura para extração de informações sobre funções.
 * `function_traits` completa as estruturas de _traits_ de funções da biblioteca
 * padrão do C++ fornecendo fácil acesso a informações sobre o tipo de retorno,
 * aridade e tipo de cada parâmetro da função.
 *
 * `function_traits` possui os sequintes tipos-membro:
 * @li **return_type** - tipo retornado pela função
 * @li **arity** - número de parâmetros esperados pela função
 * @li **argument\<i\>** - estrutura _template_ com um tipo-membro `type` que
 *                       define o tipo do parâmetro `i` da função.
 *
 * Retirado de http://functionalcpp.wordpress.com/2013/08/05/function-traits/
 */


namespace functional {


// Forward declaration
template<class F>
struct function_traits;


///@name Traits de funções gerais
//@{

/**
 * Versão geral para funções
 * @see FunctionTraits.hpp
 */
template<class R, class... Args>
struct function_traits<R(Args...)>
{
    using return_type = R;

    static const std::size_t arity = sizeof...(Args);

    template <std::size_t N>
    struct argument
    {
	    static_assert(N < arity, "error: invalid parameter index.");
	    using type = typename std::tuple_element<N,std::tuple<Args...>>::type;
    };
};


/**
 * Especialização para ponteiros para funções.
 * Extrai o tipo da função a partir do ponteiro.
 * @see FunctionTraits.hpp
 */
template<class R, class... Args>
struct function_traits<R(*)(Args...)> : public function_traits<R(Args...)>
{};

//@}


///@name Especializações de traits para funções-membro
//@{

/**
 * Especialização para ponteiro para funções-membro.
 * Inclui o parâmetro relativo ao `this`.
 */
template<class C, class R, class... Args>
struct function_traits<R(C::*)(Args...)> : public function_traits<R(C&,Args...)>
{};

/**
 * Especialização para ponteiros para funções-membro constantes.
 * Inclui o parâmetro relativo ao `this`.
 * @see FunctionTraits.hpp
 */
template<class C, class R, class... Args>
struct function_traits<R(C::*)(Args...) const> : public function_traits<R(C&,Args...)>
{};

/**
 * Especialização para ponteiros para objetos-membro
 * Considera uma função que recebe o `this` e retorna o tipo do objeto-membro.
 * @see FunctionTraits.hpp
 */
template<class C, class R>
struct function_traits<R(C::*)> : public function_traits<R(C&)>
{};

//@}


///@name Traits de functors
//@{

/**
 * Versão geral para functors.
 * @see FunctionTraits.hpp
 */
template<class F>
struct function_traits
{
private:
	using call_type = function_traits<decltype(&F::operator())>;
public:
	using return_type = typename call_type::return_type;

	static const std::size_t arity = call_type::arity - 1;

	template <std::size_t N>
	struct argument
	{
		static_assert(N < arity, "error: invalid parameter index.");
		using type = typename call_type::template argument<N+1>::type;
	};
};

/**
 * Especialização para referências para functors.
 * Extrai o tipo a partir da referencia.
 * @see FunctionTraits.hpp
 */
template<class F>
struct function_traits<F&> : public function_traits<F>
{};

/**
 * Especialização para referências _r-value_ para functors.
 * Extrai o tipo a partir da referencia _r-value_.
 * @see FunctionTraits.hpp
 */
template<class F>
struct function_traits<F&&> : public function_traits<F>
{};

//@}


}  // namespace functional


#endif  // FUNCTIONAL_FUNCTIONTRAITS_HPP
