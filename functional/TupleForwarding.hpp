#ifndef FUNCTIONAL_TUPLEFORWARDING_HPP
#define FUNCTIONAL_TUPLEFORWARDING_HPP


/**
 * @file TupleForwarding.hpp
 * Estruturas de expansão de tuplas para parâmetros de função.
 * Inspirado em http://functionalcpp.wordpress.com/2013/08/28/tuple-forwarding/.
 */


#include "Eval.hpp"

#include "util/IntegerSequence.hpp"

#include <tuple>
#include <functional>  // Para bind


namespace functional {


namespace details {

template<class Func, class ArgsTuple, unsigned int... indices>
auto tuple_eval_expansion( Func&& f,
                           ArgsTuple&& argsTuple,
                           util::integer_sequence<indices...> )
-> decltype( eval( std::forward<Func>(f), std::get<indices>(argsTuple)... ) )
{
    return eval( std::forward<Func>(f),
	             std::get<indices>( std::forward<ArgsTuple>(argsTuple) )... );
}

}  // namespace details


/**
 * Invoca `f`, expandindo `argsTuple` em argumentos para a função.
 *
 * A execução de `f` é feita usando eval().
 */
template<class Func, class ArgsTuple>
auto tuple_eval(Func&& f, ArgsTuple&& argsTuple)
-> decltype( details::tuple_eval_expansion(
                 std::forward<Func>(f),
                 std::forward<ArgsTuple>(argsTuple),
                 util::make_index_sequence<std::tuple_size<typename std::remove_reference<ArgsTuple>::type>::value>()
                 )
             )
{
	using Tuple = typename std::remove_reference<ArgsTuple>::type;

	return details::tuple_eval_expansion(
	            std::forward<Func>(f),
	            std::forward<ArgsTuple>(argsTuple),
	            util::make_index_sequence<std::tuple_size<Tuple>::value>()
	            );
}


}  // namespace functional


#endif  // FUNCTIONAL_TUPLEFORWARDING_HPP
