#ifndef PETRINETIMPL_HPP
#define PETRINETIMPL_HPP


#include "PetriNet.hpp"

#include "util/MakeRange.hpp"


///@{
/// @internal
/// Macros para auxiliar a definição de membros da classe PetriNet fora da
/// definição da classe em si.
/// Exemplo:
/// @code
/// PETRI_NET_TEMPLATE
/// int PETRI_NET_CLASS::funcaoMembro( int a, int b ) {
///     ...;  // Definição
/// }
/// @endcode

// ATENÇÃO: Essas macros precisam ser "desdefinidas" no final deste arquivo!

#define PETRI_NET_TEMPLATE  \
	template<typename Place, typename Transition>

#define PETRI_NET_CLASS PetriNet<Place, Transition>

///@}


namespace petriNet {


PETRI_NET_TEMPLATE
template<typename TransitionType,
         typename... InputPlacesRefs,
         typename... OutputPlacesRefs>
typename PETRI_NET_CLASS::TransitionRef
PETRI_NET_CLASS::addTransition( TransitionType && transition,
                                std::tuple<InputPlacesRefs...> inputPlaces,
                                std::tuple<OutputPlacesRefs...> outputPlaces )
{
	// Assertivas de entrada:
	using TransitionTraits = traits::TransitionTraits<TransitionType>;
	using ResultsPack = typename TransitionTraits::ResultsPack;

	static const size_t inputPlacesSize = sizeof...(InputPlacesRefs);
	static_assert( inputPlacesSize == TransitionTraits::arity,
	        "Number of Input Places does not match Transition parameters arity." );

	static const size_t outputPlacesSize = sizeof...(OutputPlacesRefs);
	static_assert( outputPlacesSize == std::tuple_size<ResultsPack>::value,
	        "Number of Output Places does not match Transition results arity." );


	// Cria vértice da Transição:
	const auto vertex = boost::add_vertex( graph );
	graph[vertex] = brokers::adaptToStore<Transition>( transition );
	const SpecificTransitionRef<TransitionType> transitionRef( graph, vertex );


	// Cria arestas entre a Transição e as Posições de Entrada e Saída:
	/* TODO: Incluir na aresta (via ArcTraits?):
	 * - O identificador do parâmetro (nome, ou índice posicional).
	 * - O tipo esperado/fornecido pela Transição? (a Cor das Posições deve ser
	 *   compatível com esses tipos, mas não necessariamente igual)
	 */
	using util::make_index_sequence;
	addInputEdges( transitionRef, inputPlaces, make_index_sequence<inputPlacesSize>() );
	addOutputEdges( transitionRef, outputPlaces, make_index_sequence<outputPlacesSize>() );


	// Cria função auxiliar para disparo da Transição:
	auto doFire =
	        std::bind(
	            &PetriNet::doFire<TransitionType,
	                              std::tuple<InputPlacesRefs...>,
	                              std::tuple<OutputPlacesRefs...>>,
	            this,
	            transitionRef,
	            std::move( inputPlaces ),
	            std::move( outputPlaces )
	            );

	transitionFiringFunctions[transitionRef] = std::move( doFire );


	return transitionRef;
}


PETRI_NET_TEMPLATE
void PETRI_NET_CLASS::clearMarking( )
{
	for ( auto it = vertices(graph); it.first != it.second; ++it.first )
	{
		Place * const place = boost::get<Place>( &(*it.first) );
		if ( place )
			traits::PlaceTraits<Place>::clearTokens( *place );
	}
}


PETRI_NET_TEMPLATE
std::vector<typename PETRI_NET_CLASS::PlaceRef>
PETRI_NET_CLASS::places( )
{
	std::vector<PlaceRef> places;

	const auto vertices = boost::vertices(graph);

	for ( const auto & v : util::makeRange( vertices ) )
	{
		Place * const place = boost::get<Place>( & graph[v] );
		if ( place )
			places.push_back( PlaceRef(graph, v) );
	}

	return places;
}



PETRI_NET_TEMPLATE
std::vector<typename PETRI_NET_CLASS::ConstPlaceRef>
PETRI_NET_CLASS::places( ) const
{
	std::vector<PlaceRef> places;

	const auto vertices = boost::vertices(graph);

	for ( const auto & v : util::makeRange( vertices ) )
	{
		Place * const place = boost::get<Place>( & graph[v] );
		if ( place )
			places.push_back( ConstPlaceRef(graph, v) );
	}

	return places;
}


PETRI_NET_TEMPLATE
std::vector<typename PETRI_NET_CLASS::TransitionRef>
PETRI_NET_CLASS::transitions( )
{
	std::vector<TransitionRef> transitions;

	const auto vertices = boost::vertices(graph);

	for ( const auto & v : util::makeRange(vertices) )
	{
		Transition * const transition = boost::get<Transition>( & graph[v] );
		if ( transition )
			transitions.push_back( TransitionRef(graph, v) );
	}

	return transitions;
}


PETRI_NET_TEMPLATE
std::vector<typename PETRI_NET_CLASS::ConstTransitionRef>
PETRI_NET_CLASS::transitions( ) const
{
	std::vector<ConstTransitionRef> transitions;

	const auto vertices = boost::vertices(graph);

	for ( const auto & v : util::makeRange(vertices) )
	{
		const Transition * const transition = boost::get<const Transition>( & graph[v] );
		if ( transition )
			transitions.push_back( ConstTransitionRef(graph, v) );
	}

	return transitions;
}


PETRI_NET_TEMPLATE
bool PETRI_NET_CLASS::isEnabled( const TransitionRef & transition ) const
{
	const auto inArcs = boost::in_edges( transition, graph );
	for ( const auto inArc : util::makeRange( inArcs ) )
	{
		const auto sourcePlace = boost::source( inArc, graph );
		const auto sourcePlaceRef = ConstPlaceRef( this->graph, sourcePlace );

		const auto & hasTokensFunction =
		        this->placeHasTokenFunctions.at( sourcePlaceRef );

		if ( ! hasTokensFunction() )
			return false;
	}

	return true;
}


PETRI_NET_TEMPLATE
void PETRI_NET_CLASS::execute( )
{
	const auto transitions = this->transitions();

	bool firedTransition;
	do {
		firedTransition = false;
		for ( const TransitionRef & transition : transitions )
		{
			if ( this->isEnabled( transition ) )
			{
				const bool success = this->fireTransition( transition );
				if ( ! success )
					return;  // TODO: Rever como tratar esse erro.

				firedTransition = true;
			}
		}
	} while( firedTransition );
}


PETRI_NET_TEMPLATE
bool PETRI_NET_CLASS::fireTransition( TransitionRef transition )
{
	try {
		/* TODO: Implementar semântica transacional (commit/rollback). Ex.:
		 * auto inboundArcs = this->inboundArcs( transition );
		 * st::map<ArcRef, Token> consumedTokens = consumeTokens( inboundArcs );
		 * validadeConsumedTokens( transition, consumedTokens );  // may throw
		 * std::map<ArcRef, Token> producedTokens = transition.fire( consumedTokens );
		 * validateProducedTokens( transition, producedTokens );  // may throw
		 * commit( consumedTokens, producedTokens );
		 */
		transitionFiringFunctions.at( transition )();
		return true;
	} catch ( const std::exception & /*e*/ ) {
		// TODO: rollback( consumedTokens, producedTokens, transition );
		return false;
	}
}


PETRI_NET_TEMPLATE
template<typename TransitionType,
         typename InputPlacesRefsTuple,
         typename OutputPlacesRefsTuple>
void
PETRI_NET_CLASS::doFire( const SpecificTransitionRef<TransitionType> & transition,
                         const InputPlacesRefsTuple & inputPlaces,
                         const OutputPlacesRefsTuple & outputPlaces )
{
	using std::tuple_size;
	using util::make_index_sequence;

	const auto InputPlacesSize = tuple_size<InputPlacesRefsTuple>::value;
	const auto OutputPlacesSize = tuple_size<OutputPlacesRefsTuple>::value;

	auto results = doFire_invoke( * transition,
	                              inputPlaces,
	                              make_index_sequence<InputPlacesSize>() );
	doFire_saveResults( results,
	                    outputPlaces,
	                    make_index_sequence<OutputPlacesSize>() );
}


PETRI_NET_TEMPLATE
template<typename TransitionType,
         typename... InputPlacesRefs,
         unsigned int... I>
typename traits::TransitionTraits<TransitionType>::ResultsPack
PETRI_NET_CLASS::doFire_invoke( TransitionType t,
                                 std::tuple<InputPlacesRefs...> inputPlaces,
                                 util::integer_sequence<I...> /*tupleIndices*/ )
{
	auto result = t( removeToken( std::get<I>( inputPlaces ) )... );
	return traits::TransitionTraits<TransitionType>::pack( std::move( result ) );
}


// Caso geral da iteração: 3 ou mais resultados faltando
// Expande o primeiro (I) e o último (N) resultado, e chama recursivamente para
// expandir os demais.
PETRI_NET_TEMPLATE
template<typename... ResultTypes,
         typename... OutputPlacesRefs,
         unsigned int I, unsigned int N, unsigned int... J>
void
PETRI_NET_CLASS::doFire_saveResults(
        std::tuple<ResultTypes...> results,
        std::tuple<OutputPlacesRefs...> outputPlaces,
        util::integer_sequence<I, J..., N> /*tupleIndices*/ )
{
	addToken( std::get<I>(outputPlaces), std::get<I>(results) );
	doFire_saveResults( results, outputPlaces, util::integer_sequence<J...>() );
	addToken( std::get<N>(outputPlaces), std::get<N>(results) );
}


// Caso-base da iteração: Apenas um resultado faltando.
PETRI_NET_TEMPLATE
template<typename... ResultTypes,
         typename... OutputPlacesRefs,
         unsigned int I>
void
PETRI_NET_CLASS::doFire_saveResults(
        std::tuple<ResultTypes...> results,
        std::tuple<OutputPlacesRefs...> outputPlaces,
        util::integer_sequence<I> /*tupleIndices*/ )
{
  addToken( std::get<I>(outputPlaces), std::get<I>(results) );
}


// Caso-base da iteração: Nenhum resultado faltando.
PETRI_NET_TEMPLATE
template<typename... ResultTypes, typename... OutputPlacesRefs>
void
PETRI_NET_CLASS::doFire_saveResults(
        std::tuple<ResultTypes...> /*results*/,
        std::tuple<OutputPlacesRefs...> /*outputPlaces*/,
        util::integer_sequence<> /*tupleIndices*/ )
{ }


}  // namespace petriNet


// "Desdefinição" das macros auxiliares desse arquivo
#undef PETRI_NET_TEMPLATE
#undef PETRI_NET_CLASS


#endif // PETRINETIMPL_HPP
