#ifndef PETRINET_HPP
#define PETRINET_HPP


#include "PetriNetBrokers.hpp"
#include "PetriNetTraits.hpp"

#include <boost/graph/adjacency_list.hpp>

#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>

#include <util/IntegerSequence.hpp>
#include <util/IteratorHelper.hpp>

#include <map>
#include <utility>  // std::move()


namespace petriNet {


/*
 * Item			Elementary Net			Classic Nets			Coloured Nets
 * Token		bool {true}				bool {true}				Token
 * Place		bool					int						std::multiset<Token>
 * Transition	std::function<void ()>	std::function<void ()>	std::function<??? (???)>
 * Edge
 */
/**
 * Representa uma Rede de Petri com extensão de cor nos _tokens_ e posições.
 *
 * Arcos entre Posições e Transições são criados automaticamente quando uma
 * Transições é adicionada via addTransaction(). Não é possível criar Arcos
 * manualmente.
 *
 * @section petriNet_PetriNet__traits Traits
 * PetriNet armazena as Posições e Transições de forma genérica, através dos
 * parâmetros _template_ `Place` e `Transition`. Para manipulá-los, PetriNet usa
 * as seguintes classes de _traits_:
 *
 * @li Store e Retrieve
 *     Usadas quando for necessário converter entre os tipos específicos de
 *     Posições e Transições e os tipos armazenadores de PetriNet `Place` e
 *     `Transition`.
 * @li PlaceTraits e TransitionTraits
 *     Usadas para, dado um tipo específico de Posição ou Transição, recuperar
 *     informações sobre ele e realizar algumas açoes, tal como adicionar ou
 *     remover um _token_ ou disparar a Transição.
 *
 * Para cada tipo específico de Posição e Transição armazenados na PetriNet,
 * deve haver versões adequadas dessas classes de _traits_. A falta de uma
 * classe de _trait_ leva a erros de compilação.
 *
 * Algumas especializações dessas classes já são providas para os cenários de
 * uso mais comuns. Veja PetriNetTraits.hpp e PetriNetBrokers.hpp para mais
 * informações.
 *
 * @tparam Place - Tipo de uma Posição na rede. Deve ser um tipo para o qual
 *                 possa ser atribuído cada tipo específico de Posição que será
 *                 adicionada à rede (por exemplo: Um ponteiro, de preferência
 *                 _smart pointer_, da classe base de uma hierarquia de classes
 *                 ou um tipo agregado, tal como `boost::variant`).
 * @tparam Transition - Tipo de uma Transição na rede. Deve ser um tipo para o
 *                      qual possa ser atribuído cada tipo específico de
 *                      Transição que será adicionada à rede (por exemplo:
 *                      std::function<>).
 */
template<typename Place, typename Transition>
class PetriNet
{
	/// @internal
	/// Tipo do grafo interno usado para representar a Rede de Petri.
	typedef boost::adjacency_list<
	    boost::vecS,
	    boost::vecS,
	    boost::bidirectionalS,
	    boost::variant<Place, Transition>
	> Graph;

	/// @internal
	/// _Alias_ para descritor de vértice para o tipo Graph.
	typedef typename Graph::vertex_descriptor VertexRef;


	/**
	 * Referencia um elemento da Rede de Petri (Posição ou Transição).
	 *
	 * A Posição ou Transição referenciada pode ser obtida usando semântica de
	 * ponteiros (tal como os iteradores da STL).
	 *
	 * @internal
	 * Objetos GraphVertexRef são construídos a partir de VertexRef retornados
	 * pelos métodos da Boost Graph Library sobre o grafo que representa a Rede
	 * de Petri. GraphVertexRef pode ser implicitamente convertido de volta para
	 * o VertexRef.
	 * @endinternal
	 *
	 * @tparam Referenced - tipo de vértice da Rede de Petri (Place ou
	 *                      Transition)
	 */
	template<typename Referenced>
	struct GraphVertexRef
	{
		/// Converte por cópia uma referência não-const para uma const.
		template<typename NonConstReferenced,
		         typename = typename util::EnableIfNonConstToConst<
		                                       NonConstReferenced, Referenced
		                                   >::type
		         >
		GraphVertexRef( const GraphVertexRef<NonConstReferenced> & o ) :
		    graph( o.graph ),
		    vertexRef( o.vertexRef )
		{ }

		/// Converte por _move_ uma referência não-const para uma const.
		template<typename NonConstReferenced,
		         typename = typename util::EnableIfNonConstToConst<
		                                       NonConstReferenced, Referenced
		                                   >::type
		         >
		GraphVertexRef( GraphVertexRef<NonConstReferenced> && o ) :
		    graph( std::move(o.graph) ),
		    vertexRef( std::move(o.vertexRef) )
		{ }


		/// Acessa o elemento da Rede de Petri referenciado
		Referenced & operator*() const {
			return boost::get<Referenced>( (*graph)[vertexRef] );
		}

		/// Acessa uma função ou atributo membros do elemento referenciado
		Referenced * operator->() const {
			return & boost::get<Referenced>( (*graph)[vertexRef] );
		}


		template<typename OtherReferenced,
		         typename = typename util::EnableIfSameExceptConst<Referenced, OtherReferenced>::type >
		bool operator==( const GraphVertexRef<OtherReferenced> & o ) const
		{
			return this->graph == o.graph &&
			       this->vertexRef == o.vertexRef;
		}

		template<typename OtherReferenced,
		         typename = typename util::EnableIfSameExceptConst<Referenced, OtherReferenced>::type >
		bool operator!=( const GraphVertexRef<OtherReferenced> & o ) const
		{
			return ! this->operator ==( o );
		}

	protected:
		using GraphType = typename util::TypeBroker<Referenced, Graph>::type;

		GraphVertexRef( GraphType & graph, VertexRef vertexRef ) :
		    graph( & graph ),
		    vertexRef( std::move( vertexRef ) )
		{
			// Garante que o tipo do vértice no grafo bate com `Referenced`
			assert( graph[vertexRef].type() == typeid(Referenced) );
		}

	private:
		GraphType * graph;
		VertexRef vertexRef;

#ifdef PETRINET_TEST
	/* Solução de contorno para uso de referências em EXPECT_*() do Google Test:
	 * O mecanismo de impressão do Google Test tenta descobrir se as classes são
	 * conversíveis para inteiro. Nesse momento, ele tenta acessar o operador
	 * VertexRef() declarado como privado e dá erro de compilação.
	 * Tentamos sobrescrever a lógica de impressão definindo operator<<() ou
	 * PrintTo(), mas não funcionou porque ambos os mecanismos dependem do ADL
	 * (Argument-dependent name lookup), que não funciona direito com tipos
	 * template e aninhados.
	 * Tentamos também uma abordagem baseada em friend, mas como a classe que
	 * faz o acesso é interna do Google Test e também template, ficou complicado
	 * declarar o friend.
	 * A solução, assim, foi, na compilação dos testes, tornar o operador de
	 * conversão para VertexRef público.
	 */
	public:
#endif
		operator VertexRef( ) const { return vertexRef; }

		friend class PetriNet;  // Acesso principalemnte ao consturtor

		template<class T>
		friend struct std::less;  // Acesso a cast para VertexRef, que possui `<`
	};


public:
	/// Referência para uma Posição na Rede de Petri
	typedef GraphVertexRef<Place> PlaceRef;
	/// Referência para uma Transição na Rede de Petri
	typedef GraphVertexRef<Transition> TransitionRef;

	/// Referência para uma Posição na Rede de Petri
	typedef GraphVertexRef<const Place> ConstPlaceRef;
	/// Referência para uma Transição na Rede de Petri
	typedef GraphVertexRef<const Transition> ConstTransitionRef;


	/// Referência para uma Posição de um tipo específico na Rede de Petri
	template<typename PlaceType>
	struct SpecificPlaceRef : public GraphVertexRef< typename util::TypeBroker<PlaceType, Place>::type >
	{
		template<typename NonConstPlaceType,
		         typename = typename util::EnableIfNonConstToConst<
		                                       NonConstPlaceType, PlaceType
		                                   >::type
		         >
		SpecificPlaceRef( const SpecificPlaceRef<NonConstPlaceType> & o ) :
		    PlaceRef( o )
		{ }

		template<typename NonConstPlaceType,
		         typename = typename util::EnableIfNonConstToConst<
		                                       NonConstPlaceType, PlaceType
		                                   >::type
		         >
		SpecificPlaceRef( SpecificPlaceRef<NonConstPlaceType> && o ) :
		    PlaceRef( std::move( o ) )
		{ }

		/// Acessa o elemento da Rede de Petri referenciado
		PlaceType & operator*() const {
			Place & place = PlaceRef::operator *();
			return brokers::retrieve<PlaceType>( place );
		}

		/// Acessa uma função ou atributo membros do elemento referenciado
		PlaceType * operator->() const {
			return & this->operator *();
		}

	private:
		using PlaceRef =
		    GraphVertexRef< typename util::TypeBroker<PlaceType, Place>::type >;

		SpecificPlaceRef( typename PlaceRef::GraphType & graph,
		                   VertexRef vertexRef ) :
		    PlaceRef( graph, vertexRef )
		{ }

		friend class PetriNet;  // Acesso ao construtor
	};

	template<typename TransitionType>
	struct SpecificTransitionRef : public GraphVertexRef< typename util::TypeBroker<TransitionType, Transition>::type >
	{
		TransitionType & operator*() const {
			Transition * const transition = TransitionRef::operator ->();
			return brokers::retrieve<TransitionType>( *transition );
		}

		TransitionType * operator->() const {
			return & this->operator *();
		}

	private:
		using TransitionRef =
		    GraphVertexRef< typename util::TypeBroker<TransitionType, Transition>::type >;

		SpecificTransitionRef( typename TransitionRef::GraphType & graph,
		                       VertexRef vertexRef ) :
		    TransitionRef( graph, vertexRef )
		{ }

		friend class PetriNet;
	};

	typedef typename Graph::edge_descriptor ArcRef;

	///@name Definição da estrutura
	//@{

	// TODO: Rever se mantém essa versão genérica de addPlace().
	// Ela é interessante para Place trivial (int, ou std::multiset<int>
	// basicamente). Ela é ruim, porém, se causar ambiguidade (por exemplo, se
	// Place for boost::variant).
	SpecificPlaceRef<Place> addPlace( )
	{
		return this->addPlace( Place() );
	}

	/**
	 * Adiciona uma Posição na Rede de Petri.
	 *
	 * `PlaceType` deve ser compatível com `Place` da Rede de Petri, de forma
	 * que a expressão
	 *
	 *     Place p = place;
	 *
	 * seja bem formada.
	 */
	template<typename PlaceType>
	SpecificPlaceRef<PlaceType> addPlace( PlaceType && place )
	{
		const auto vertex = boost::add_vertex( graph );
		graph[vertex] = brokers::adaptToStore<Place>( std::forward<PlaceType>( place ) );

		auto placeRef = SpecificPlaceRef<PlaceType>( graph, vertex );

		auto hasTokensFunction =
		        std::bind(
		            & doHasToken<PlaceType>,
		            placeRef
		            );

		this->placeHasTokenFunctions[placeRef] = std::move(hasTokensFunction);

		return placeRef;
	}


	/**
	 * Adiciona uma Transição na Rede de Petri.
	 *
	 * Arcos entre as Posições de Entrada e de Saída são criados
	 * automaticamente.
	 *
	 * O número de `inputPlaces` deve ser igual à aridade da Transição tal como
	 * definido em TransitionTraits::arity. Da mesma forma, o número de
	 * `outputPlaces` deve ser igual ao tamanho de
	 * TransitionTraits::ResultsPack.
	 */
	template<typename TransitionType,
	         typename... InputPlacesRefs,
	         typename... OutputPlacesRefs>
	TransitionRef addTransition(
	        TransitionType&& transition,
	        std::tuple<InputPlacesRefs...> inputPlaces,
	        std::tuple<OutputPlacesRefs...> outputPlaces );

	//@}

	///@name Definição da marcação
	//@{

	/**
	 * Remove todos os _tokens_ das Posições da Rede de Petri.
	 *
	 * Esse método invoca PlaceTraits::clearTokens() para cada Posição da rede.
	 */
	void clearMarking( );

	/* TODO: Criar métodos {get,set}Marking()?
	 *       Podem ser úteis para "persistência" da rede, ou mesmo para algo do
	 *       tipo "resetMarking()".
	 * ??? getMarking( ) const;
	 * void setMarking( );
	 */

	/**
	 * Adiciona `token` à Posição.
	 *
	 * A Cor do _token_ deve ser compatível com a Cor da Posição, de forma que
	 * a expressão
	 *
	 *     PlaceTraits<PlaceType>::addToken( typedPlace, token );
	 *
	 * é bem formada para `typedPlace` do tipo `PlaceType`.
	 *
	 * @todo Documentar comportamento em casos de erro (ver TODOs em PlaceTraits).
	 */
	template<typename PlaceType, typename Colour>
	void addToken( SpecificPlaceRef<PlaceType> place, Colour token )
	{
		assert( place.graph == &this->graph );
		traits::PlaceTraits<PlaceType>::addToken( *place, token );
	}

	/**
	 * Remove um _token_ da Posição.
	 *
	 * @throws NoToken
	 */
	template<typename PlaceType>
	typename traits::PlaceTraits<PlaceType>::Colour
	removeToken( SpecificPlaceRef<PlaceType> place )
	{
		assert( place.graph == &this->graph );
		return traits::PlaceTraits<PlaceType>::removeToken( *place );  // May throw NoToken
	}

	//@}

	///@name Consulta
	//@{

	std::vector<PlaceRef> places();
	std::vector<ConstPlaceRef> places() const;

	std::vector<TransitionRef> transitions();
	std::vector<ConstTransitionRef> transitions() const;

	bool isEnabled( const TransitionRef & transition ) const;

	// Placeholders de possíveis métodos futuros (serão criados caso necessário)
//	std::vector<PlaceRef> inputPlaces( TransitionRef ) const;
//	std::vector<PlaceRef> outputPlaces( TransitionRef ) const;

//	std::vector<TransitionRef> sourceTransitions( PlaceRef ) const;
//	std::vector<TransitionRef> targetTransitions( PlaceRef ) const;

//	std::vector<ArcRef> inboundArcs( VertexRef ) const;
//	std::vector<ArcRef> outboundArcs( VertexRef ) const;

	//@}

	///@name Execução
	//@{

	std::vector<TransitionRef> enabledTransitions( ) const;

	/**
	 * @brief Executa toda a rede até não haverem mais transações habilitadas
	 * @see firetransition()
	 */
	void execute( );

	/**
	 * @brief Dispara a transição habilitada, equivalendo a um passo da execução
	 *        da rede
	 * @param transition - Transição habilitada a ser disparada
	 * @returns `true` se a Transição foi disparada com sucesso
	 * @see execute()
	 */
	bool fireTransition( TransitionRef transition );

	//@}


private:
	Graph graph;

	// TODO: Rever containers e índices (desempenho, correture, genericidade etc.)
	std::map<TransitionRef, std::function<void ()>> transitionFiringFunctions;

	std::map<ConstPlaceRef, std::function<bool ()>> placeHasTokenFunctions;

	/* TODO: Implementar semântica transacional no disparo de Transições
	 * void commitFiredTransition( );
	 * void rollbackFiredTransition( );
	 */


private:

	///@name Métodos internos auxiliares da lógica de addTransition()
	//@{

	template<typename... InputPlacesRefs, unsigned int I, unsigned int... N>
	void addInputEdges( const TransitionRef & transition,
	                    const std::tuple<InputPlacesRefs...> & inputPlaces,
	                    util::integer_sequence<I, N...> /*tupleIndices*/ )
	{
		boost::add_edge( std::get<I>( inputPlaces ), transition, graph );
		addInputEdges( transition, inputPlaces, util::integer_sequence<N...>());
	}

	template<typename... InputPlacesRefs>
	void addInputEdges( const TransitionRef & /*transition*/,
	                    const std::tuple<InputPlacesRefs...> & /*inputPlaces*/,
	                    util::integer_sequence<> /*tupleIndices*/ )
	{ }


	template<typename... OutputPlacesRefs, unsigned int I, unsigned int... N>
	void addOutputEdges( const TransitionRef & transition,
	                    const std::tuple<OutputPlacesRefs...> & outputPlaces,
	                     util::integer_sequence<I, N...> /*tupleIndices*/ )
	{
		boost::add_edge( transition, std::get<I>( outputPlaces ), graph );
		addInputEdges( transition, outputPlaces, util::integer_sequence<N...>());
	}

	template<typename... OutputPlacesRefs>
	void addOutputEdges( const TransitionRef & /*transition*/,
	                     const std::tuple<OutputPlacesRefs...> & /*inputPlaces*/,
	                     util::integer_sequence<> /*tupleIndices*/ )
	{ }


	template<typename PlaceType>
	static bool doHasToken( const SpecificPlaceRef<PlaceType> & place )
	{
		return traits::PlaceTraits<PlaceType>::hasToken( * place );
	}

	template<typename TransitionType,
	         typename InputPlacesRefsTuple,
	         typename OutputPlacesRefsTuple>
	void doFire( const SpecificTransitionRef<TransitionType> & transition,
	             const InputPlacesRefsTuple & inputPlaces,
	             const OutputPlacesRefsTuple & outputPlaces );


	template<typename TransitionType,
	         typename... InputPlacesRefs,
	         unsigned int... I>
	typename traits::TransitionTraits<TransitionType>::ResultsPack
	doFire_invoke( TransitionType t,
	                    std::tuple<InputPlacesRefs...> inputPlaces,
	                    util::integer_sequence<I...> /*tupleIndices*/ );


	template<typename... ResultTypes,
	         typename... OutputPlacesRefs,
	         unsigned int I, unsigned int N, unsigned int... J>
	void doFire_saveResults( std::tuple<ResultTypes...> results,
	                        std::tuple<OutputPlacesRefs...> outputPlaces,
	                        util::integer_sequence<I, J..., N> /*tupleIndices*/ );

	template<typename... ResultTypes,
	         typename... OutputPlacesRefs,
	         unsigned int I>
	void doFire_saveResults( std::tuple<ResultTypes...> results,
	                        std::tuple<OutputPlacesRefs...> outputPlaces,
	                        util::integer_sequence<I> /*tupleIndices*/ );

	template<typename... ResultTypes,
	         typename... OutputPlacesRefs>
	void doFire_saveResults( std::tuple<ResultTypes...> /*results*/,
	                        std::tuple<OutputPlacesRefs...> /*outputPlaces*/,
	                        util::integer_sequence<> /*tupleIndices*/ );

	//@}

};


}  // namespace petriNet


#include "PetriNet-impl.hpp"  // TODO: Rever se mantém esse include aqui


#endif // PETRINET_HPP
