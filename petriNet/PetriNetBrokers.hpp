#ifndef PETRINET_PETRINETBROKERS_HPP
#define PETRINET_PETRINETBROKERS_HPP


// Para especializações para std::function:
#include <functional>

// Para a especialização para std::shared_ptr:
#include <memory>

// Para especializações para boost::variant:
#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>


/**
 * Estruturas de _traits_ de PetriNet para interoperar entre os tipos
 * específicos e armazenados de Posições e Transições.
 *
 * São definidas duas estruturas de _traits_ nesse namespace: PlaceTraits e
 * TransitionTraits. Também são providas duas funções auxiliares para
 * aplicá-las: adaptToStore() e retrieve().
 *
 * @see ::petriNet::traits
 */
namespace petriNet {
namespace brokers {


/**
 * Adapta o tipo concreto de Posição ou Transição para o tipo armazenado na
 * PetriNet.
 *
 * Essas funções são chamadas pelos métodos PetriNet::addPlace() e
 * PetriNet::addTransition() para adaptar o tipo concreto da Posição ou
 * Transição passados como parâmetros para o tipo armazenado pela rede de Petri
 * (parâmetro template `Place`).
 */
template<typename PetriNetType, typename ConcreteType>
struct Store
{
	static PetriNetType adaptToStore( ConcreteType && o )
	{
		return PetriNetType( std::forward<ConcreteType>( o ) );
	}
};

/**
 * Especialização para armazenamento de std::shared_ptr.
 *
 * Essa especialização cria um novo objeto ConcreteType alocado na memória,
 * copiado ou movido do objeto original, armazenando o novo objeto em um
 * std::shared_ptr.
 */
template<typename T, typename ConcreteType>
struct Store<std::shared_ptr<T>, ConcreteType>
{
	static std::shared_ptr<T> adaptToStore( ConcreteType && o )
	{
		return std::make_shared<typename std::decay<ConcreteType>::type>( std::forward<ConcreteType>( o ) );
	}
};


/**
 * Especialização para armazenamento de boost::variant.
 */
template<typename... Ts, typename ConcreteType>
struct Store<boost::variant<Ts...>, ConcreteType>
{
	static boost::variant<Ts...> adaptToStore( ConcreteType && o )
	{
		return boost::variant<Ts...>( std::forward<ConcreteType>( o ) );
	}
};


/**
 * Recupera a Posição ou Transição armazenada na PetriNet, convertendo-a para o
 * seu tipo concreto.
 *
 * Essa estrutura **não** deve fazer cópias, mas sim trabalhar sempre com
 * referências para os objetos armazenados na rede de Petri.
 */
template<typename PetriNetType, typename ConcreteType>
struct Retrieve
{
	static ConcreteType & asConcreteType( PetriNetType & o )
	{
		return o;
	}

	static const ConcreteType & asConcreteType( const PetriNetType & o )
	{
		return o;
	}
};


/**
 * Especialização para objetos armazenados em std::shared_ptr.
 *
 * Retorna uma referência para o objeto apontado por `o`.
 */
template<typename T, typename ConcreteType>
struct Retrieve<std::shared_ptr<T>, ConcreteType>
{
	static ConcreteType & asConcreteType( const std::shared_ptr<T> & o )
	{
		auto cast = std::static_pointer_cast<ConcreteType>( o );
		return * cast;
	}
};


/**
 * Especialização para objetos armazenados em boost::variant.
 *
 * Retorna uma referência para o resultado de `boost::get<ConcreteType>( o )`.
 */
template<typename... Ts, typename ConcreteType>
struct Retrieve<boost::variant<Ts...>, ConcreteType>
{
	static ConcreteType & asConcreteType( boost::variant<Ts...> & o )
	{
		return boost::get<ConcreteType>( o );
	}

	static const ConcreteType & asConcreteType( const boost::variant<Ts...> & o )
	{
		return boost::get<const ConcreteType>( o );
	}
};


/**
 * Especialização para objetos armazenados em std::function.
 *
 * Retorna uma referência para o objeto funcional armazenado dentro do
 * std::function (isto é, `o.target<ConcreteType>()`).
 */
template<typename R, typename... Args, typename ConcreteType>
struct Retrieve<std::function<R (Args...)>, ConcreteType>
{
	static ConcreteType & asConcreteType( std::function<R (Args...)> & o )
	{
		auto target = o.template target<ConcreteType>();
		return * target;
	}

	static const ConcreteType & asConcreteType( const std::function<R (Args...)> & o )
	{
		auto target = o.template target<ConcreteType>();
		return * target;
	}
};


/**
 * Seleciona e aplica a versão de Store adequada para  o objeto `o`.
 */
template<typename PetriNetType, typename ConcreteType>
auto adaptToStore( ConcreteType&& o )
-> decltype( Store<PetriNetType, ConcreteType>::adaptToStore( std::forward<ConcreteType>( o ) ) )
{
	return Store<PetriNetType, ConcreteType>::adaptToStore( std::forward<ConcreteType>( o ) );
}


/**
 * Seleciona e aplica a versão de Retrieve adequada para o objeto `o`.
 */
template<typename ConcreteType, typename PetriNetType>
auto retrieve( PetriNetType & o )
-> decltype( Retrieve<PetriNetType, ConcreteType>::asConcreteType( o ) )
{
	return Retrieve<PetriNetType, ConcreteType>::asConcreteType( o );
}



}  // namespace brokers
}  // namespace petriNet


#endif  // PETRINET_PETRINETBROKERS_HPP
