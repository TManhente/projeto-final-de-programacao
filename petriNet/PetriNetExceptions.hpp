#ifndef PETRINETEXCEPTIONS_HPP
#define PETRINETEXCEPTIONS_HPP


/**
 * @file PetriNetExceptions.hpp
 *
 * Todas as exceções [herdam virtualmente de std::exception](http://www.boost.org/doc/libs/1_55_0/libs/exception/doc/using_virtual_inheritance_in_exception_types.html),
 * seguindo a sugestão de Boost Exception.
 *
 */

#include <stdexcept>


namespace petriNet {


class NoToken : virtual public std::logic_error
{
public:
	explicit NoToken( std::string placeName ) :
	    logic_error( std::move(placeName) )
	{ }

	explicit NoToken( const char * placeName = "" ) :
	    logic_error(placeName)
	{ }
};


}  // namespace petriNet


#endif // PETRINETEXCEPTIONS_HPP
