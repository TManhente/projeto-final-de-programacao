#ifndef PETRINETSTDTRAITS_HPP
#define PETRINETSTDTRAITS_HPP


/**
 * @file PetriNetStdTraits.hpp
 * Especializações das classes de _traits_ de PetriNet para tipos comuns.
 *
 * Este arquivo complementa os _traits_ mais genéricos em PetriNetDetails.hpp
 * com especializações para tipos comuns, tais como _containers_ da STL.
 *
 * @see ::petriNet::traits
 */

#include "PetriNetTraits.hpp"
#include "PetriNetExceptions.hpp"

#include <deque>
#include <set>


namespace petriNet {
namespace traits {


/**
 * Especialização de PlaceTraits para std::deque.
 *
 * Posições do tipo std::deque podem armazenar múltiplos _tokens_ de uma
 * determinada Cor simultaneamente.
 */
template<typename T>
struct PlaceTraits<std::deque<T>>
{
	using Colour = typename std::deque<T>::value_type;

	static T removeToken( std::deque<T> & place )
	{
		if ( place.empty( ) )
			throw NoToken();

		auto token = place.front( );
		place.pop_front( );
		return token;
	}

	template<typename Colour>
	static void addToken( std::deque<T> & place, Colour token )
	{
		place.push_back( token );
	}

	static void clearTokens( std::deque<T> & place )
	{
		place.clear( );
	}

	static bool hasToken( const std::deque<T> & place )
	{
		return ! place.empty();
	}
};


}  // namespace traits
}  // namespace petriNet


#endif // PETRINETSTDTRAITS_HPP
