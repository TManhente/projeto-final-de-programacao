#ifndef PETRINETTRAITS_HPP
#define PETRINETTRAITS_HPP


#include <tuple>  // Para std::tuple e std::tuple_size
#include <utility>  // Para std::pair

#include <functional/FunctionTraits.hpp>


/**
 * Estruturas de _traits_ de PetriNet para manipular as Posições e Transições.
 *
 * São definidas duas estruturas de _traits_ nesse namespace: PlaceTraits e
 * TransitionTraits.
 *
 * @see ::petriNet::brokers, PetriNetStdTraits.hpp
 */
namespace petriNet {
namespace traits {

/// @internal
namespace details {

template<typename T>
struct AsTuple
{
	using TupleType = std::tuple<T>;
};

template<typename T1, typename T2>
struct AsTuple<std::pair<T1, T2>>
{
	using TupleType = std::tuple<T1, T2>;
};

template<typename... Ts>
struct AsTuple<std::tuple<Ts...>>
{
	using TupleType = std::tuple<Ts...>;
};

}  // namespace details


// Documentação em PetriNetTraits.dox
template<typename Place>
struct PlaceTraits;


// Documentação em PetriNetTraits.dox
template<typename Transition>
struct TransitionTraits
{
private:
	using FunctionTraits = functional::function_traits<Transition>;
	using FunctionReturnType = typename FunctionTraits::return_type;
public:
	using ResultsPack = typename details::AsTuple<FunctionReturnType>::TupleType;
	static const auto arity = FunctionTraits::arity;

	static ResultsPack pack( FunctionReturnType result )
	{
		return ResultsPack( result );
	}
};


}  // namespace traits
}  // namespace petriNet


#endif // PETRINETTRAITS_HPP
