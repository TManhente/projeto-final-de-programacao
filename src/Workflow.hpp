#ifndef CPPWFMS_WORKFLOW_HPP
#define CPPWFMS_WORKFLOW_HPP


#include "WorkflowExceptions.hpp"

#include "structure/Data.hpp"
#include "structure/Task.hpp"

#include "petriNet/PetriNet.hpp"
#include "petriNet/PetriNetExceptions.hpp"

#include <memory>  // Para shared_ptr
#include <string>
//#include <vector>


namespace cppwfms {


//// TODO: Expor interface de grafo compatível com algoritmos da BGL
class Workflow
{
	using WfPetriNet =
	    petriNet::PetriNet< std::shared_ptr<structure::DataValue>,
	                        std::shared_ptr<structure::Task> >;

public:
	template<typename DataValue>
	using SpecificDataValueRef =
	    typename WfPetriNet::template SpecificPlaceRef<DataValue>;

	using TaskRef = WfPetriNet::TransitionRef;

	std::string getName( );

	template<typename Value>
	SpecificDataValueRef< structure::SpecificDataValue<typename std::decay<Value>::type> >
	addDataValue( std::string name, Value&& value )
	{
		using DecayedValue = typename std::decay<Value>::type;
		using DataValueType = structure::SpecificDataValue<DecayedValue>;

		DataValueType dataValue( std::move(name), std::forward<Value>(value) );

		return petriNet.addPlace( std::move(dataValue) );
	}


	template<typename Task,  // ExecutableTask
	         typename... InputDataValuesRefs >
	std::tuple<TaskRef, SpecificDataValueRef< structure::SpecificDataValue<typename Task::outputDataTypes> > >
	addTask( Task&& task,
	                 std::tuple<InputDataValuesRefs...> inputDataValues )
	{
		static_assert( Task::inputParametersArity == sizeof...(InputDataValuesRefs),
		        "Size of inputDataValues does not match task input parameters arity." );

		// NÃO! Isso deveria estar nos arcos!  // TODO: Rever, essas informações talvez precisem estar nos arcos
//		auto taskInputData = createTaskPlaces<0>(
//		                 task,
//		                 util::make_index_sequence<Task::data<0>::arity>() );

		// OK (+/-):
//		auto taskOutputData = createTaskOutputPlaces(
//		                 task,
//		                 util::make_index_sequence<Task::inputParametersArity>() );
		auto taskOutputData =  // TODO: Rever para múltiplos parâmetros de saída
		        std::make_tuple( this->addDataValue<typename Task::outputDataTypes>( "res" ) );

		//return petriNet.addTransition(
		auto transitionRef = petriNet.addTransition(
		            std::forward<Task>(task),
		            std::move(inputDataValues),
		            /*std::move(*/taskOutputData/*)*/ );

		return std::tuple_cat( std::make_tuple( std::move(transitionRef) ),
		                       std::move(taskOutputData) );
	}

//	typedef WfPetriNet::PlaceRef DataInstance;
//	typedef WfPetriNet::TransitionRef TaskInstance;

//	std::vector<DataInstance> getParameters() const;
//	std::vector<DataInstance> getFixedParameters() const;
//	std::vector<TaskInstance> getTasks() const;
//	Unknown getTransitions() const;

//	TaskInstance getInitialTask() const;
//	TaskInstance getFinalTask() const;

//	Unknown geInboundTransitions( TaskInstance ) const;
//	Unknown geOutboundTransitions( TaskInstance ) const;

	void execute()
	{
		petriNet.execute();
	}

private:
//	/* Instâncias de Workflow, por serem objetos complexos, devem ser
//	 * construidos apenas por um _builder_ que possa validar todas as
//	 * invariantes e assertivas estruturais da classe.  */
//	Workflow();

	std::string name;

//	// TODO: Rever tipo de Edge e nome do typedef
	WfPetriNet petriNet;

	template<typename Value>
	SpecificDataValueRef< structure::SpecificDataValue<typename std::decay<Value>::type>>
	addDataValue( std::string name )
	{
		using DecayedValue = typename std::decay<Value>::type;
		using DataValueType = structure::SpecificDataValue<DecayedValue>;

		DataValueType dataValue( std::move(name) );

		return petriNet.addPlace( std::move(dataValue) );
	}

#if 0
	template<typename Task, unsigned int... i>
	auto createTaskPlaces( Task & task, util::integer_sequence<i...> )
	-> decltype( std::make_tuple(
	                 this->addDataValue<typename Task::template inputDataTypes<i>::type>(
	                    task.getInputData()[i]->getName() )...
	                 )
	             )
	{
		using structure::SpecificDataValue;

		const std::vector<const structure::Data*> taskData = task.getInputData();

		return std::make_tuple(
		            this->addDataValue<typename Task::template inputDataTypes<i>::type>(
		                taskData[i]->getName() )...
		            );
	}

	template<typename Task, unsigned int... i>
	auto createTaskOutputPlaces( Task & task, util::integer_sequence<i...> )
	{
	    using structure::SpecificDataValue;

	    const std::vector<const structure::Data*> taskOutputData =
	            task.getOutputData();

	    return std::make_tuple(
	                this->addDataValue<typename Task::template outputDataTypes<i>::type>(
	                    taskOutputData[i]->getName() )...
	                );
	}

#endif
};


}  // namespace cppwfms


namespace petriNet {
namespace traits {

template<typename T>
struct PlaceTraits<cppwfms::structure::SpecificDataValue<T>>
{
	using Colour = T;

	static T removeToken( cppwfms::structure::SpecificDataValue<T> & place )
	{
		if ( ! place.hasValue() )
			throw NoToken( place.getName() );

		return place.takeValue( );
	}

	static void addToken( cppwfms::structure::SpecificDataValue<T> & place,
	                      T token )
	{
		if ( place.hasValue() )
			throw cppwfms::OverridingValue( place.getName() );  // TODO: Rever exceção

		return place.putValue( std::move(token) );
	}

	static bool hasToken( const cppwfms::structure::SpecificDataValue<T> & value )
	{
		return value.hasValue();
	}
};

}  // namespace traits
}  // namespace petriNet


#endif  // CPPWFMS_WORKFLOW_HPP
