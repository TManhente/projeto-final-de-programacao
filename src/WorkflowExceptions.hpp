#ifndef CPPWFMS_WORKFLOWEXCEPTIONS_HPP
#define CPPWFMS_WORKFLOWEXCEPTIONS_HPP


#include <stdexcept>


namespace cppwfms {


/**
 * Exceção levantada quando se tenta recuperar um dado não existente no Workflow.
 *
 * O dado pode não existir por ainda não ter sido produzido por uma Task ou por
 * já ter sido consumido pela tarefa seguinte.
 *
 * @see SpecificDataValue
 */
class NoValue : public virtual std::logic_error
{
public:
	explicit NoValue( std::string name ) :
	    std::logic_error( "Sem valor em " + std::move(name) )
	{ }
};


/**
 * Exceção levantada quando se tenta definir um valor novo para um dado no
 * Workflow que já tem um valor definido.
 *
 * @see SpecificDataValue
 */
class OverridingValue : public virtual std::logic_error
{
public:
	explicit OverridingValue( std::string name ) :
	    std::logic_error( "Sobrescrevendo valor em " + std::move(name) )
	{ }
};


}  // namespace cppwfms


#endif  // CPPWFMS_WORKFLOWEXCEPTIONS_HPP
