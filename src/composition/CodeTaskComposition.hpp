#ifndef CPPWFMS_COMPOSITION_CODETASKCOMPOSITION_HPP
#define CPPWFMS_COMPOSITION_CODETASKCOMPOSITION_HPP


#include <tuple>

#include "functional/FunctionTraits.hpp"
#include "functional/TupleForwarding.hpp"

#include "util/DemangleTypeName.hpp"

#include "structure/CodeTask.hpp"


namespace cppwfms {
namespace composition {


#if 0
//template<typename... Args>
//std::function<>
//std::createWorkflow( Args... args )
#else
// Para os workflows mais simples, análogos a chamadas de funções em código,
// o conceito de uma única saída parece válido, e por isso é válido trabalhar
// com uma topLevelTask. Talvez possamos também ter o conceito de uma tarefa
// EndWorkflow que seja o sink final de quaisquer tarefas paralelas que se
// tenha.
//void createWorklow( Task topLevelTask )
#endif
//{
	// TODO: Parse workflow elements
	//	- Extract parameters in order to compose the workflow interface
	//	- Get tasks depending only on existing places, create them and their
	//	  inboud edges from those places.
	//	- Repeat above until all tasks were composed
	//	- Generate the Petri Net?
//}


/**
 * Instância de execução de uma CodeTask para determinados parâmetros de entrada.
 *
 * A assinatura de CodeTaskInstance, isto é, os argumentos _template_ `F` e
 * `Args`, devem estar normalizadas. Isto é garantido por codeTask() através do
 * uso de std::decay, de forma similar ao que é feito internamente em std::bind.
 * Tanto `F` quanto `Args` devem ser tipos de valor tal como os resultantes das
 * conversões de tipos aplicadas a argumentos de funções passados por valor.
 *
 * @see codeTask()
 */
template<typename F, typename... Args>
class CodeTaskInstance
{
	using FunctionTraits = typename functional::function_traits<F>;
	using Result = typename FunctionTraits::return_type;
public:
	template<typename... Args2>
	CodeTaskInstance( F&& function, Args2&&... arguments_ ) :
	    function( std::forward<F>(function) ),
	    arguments( std::forward<Args2>(arguments_)... )
	{
		static_assert( FunctionTraits::arity == sizeof...(arguments_),
		               "Number of arguments differ from function arity." );
	}

	const structure::CodeTask<F> & getTask() const
	{
		static structure::CodeTask<F> task( function );
		return task;
	}

	const std::tuple< Args... > & getArguments() const
	{
		return arguments;
	}

	Result evaluate( ) const
	{
		return functional::tuple_eval( function, arguments );
	}

	// TODO: Rever este operador de cast (atualmente, é apenas para fazer o bind funcionar)
	operator Result ( ) const
	{
		return this->evaluate( );
	}

private:
	/* Esses atributos são similares aos armazenados por std::bind. Eles
	 * **DEVEM** ser decay sobre os argumentos de fato passados para codeTask(),
	 * sendo assim copiados por valor e evitando armazenar referências para
	 * objetos temporários.  */
	F function;
	typename std::tuple< Args... > arguments;

};


/**
 * Cria um objeto CodeTaskInstance para invocar `function` para os parâmetros `args`.
 */
template<typename Function, typename... Args>
CodeTaskInstance<
    typename std::decay<Function>::type,
    typename std::decay<Args>::type...
>
codeTask( Function&& function, Args&&... args )
{
	typedef typename functional::function_traits<Function> FunctionTraits;
	static_assert( FunctionTraits::arity == sizeof...(args),
	               "Number of arguments differ from function arity." );

	return CodeTaskInstance<
	        typename std::decay<Function>::type,
	        typename std::decay<Args>::type...
	        > ( std::forward<Function>(function),
	            std::forward<Args>(args)...
	            );

	// TODO: Transformação em PetriNet:
	// foreach arg in args
	//     switch( arg.type )
	//     case Task: bind resut (get output place, create inbound edge)
	//     case Param: bind (get place, create inbound edge)
	//     case other: bind literal value (no place, store internally?
	//                                     or create a fixed place that is not a
	//                                     workflow parameter?)
}


}  // namespace composition
}  // namespace cppwfms


#endif  // CPPWFMS_COMPOSITION_CODETASKCOMPOSITION_HPP
