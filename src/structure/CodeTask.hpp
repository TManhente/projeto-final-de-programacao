#ifndef CPPWFMS_STRUCTURE_CODETASK_HPP
#define CPPWFMS_STRUCTURE_CODETASK_HPP


#include "Data.hpp"
#include "Task.hpp"

#include "CodeTaskTraits.hpp"

#include <algorithm>  // Para std::transform
#include <memory>  // Para std::addressof


namespace cppwfms {
namespace structure {


/**
 * Tarefa que representa a invocação de uma função C++.
 */
template<typename T>
class CodeTask;

template<typename R, typename... Args>
class CodeTask<R (*)(Args...)> : public structure::Task
{
public:
	using FunctionType = R (*)(Args...);

	static const size_t inputParametersArity = sizeof...(Args);
	static const size_t outputParametersArity = 1;  // TODO: Rever esses valores (usar function_traits)

	template<size_t i>
	using inputDataTypes = typename functional::function_traits<FunctionType>::template argument<i>;

	// template<size_t i>  // TODO: Rever questão de outputDataTypes em relação à petriNet::TransitionTraits
	using outputDataTypes = R;

	CodeTask( FunctionType function ) :
	    function( function ),
	    inputData( createInputData(function,
	                               util::make_index_sequence<sizeof...(Args)>()) ),
	    outputData( traits::codeTaskOutputDataName<0>( function ), typeid(R) )  // TODO: Rever
	{
	}

	std::string getName() const
	{
		return traits::codeTaskName( function );
	}

	std::vector<const structure::Data*> getInputData() const
	{
		using structure::Data;

		std::vector<const Data*> inputData;
		inputData.reserve( this->inputData.size() );

		std::transform( this->inputData.begin(), this->inputData.end(),
		                std::inserter( inputData, inputData.begin()),
		                [](const Data& input) { return & input; } );

		return inputData;
	}

	std::vector<const structure::Data*> getOutputData() const
	{
		// TODO: Rever
		return std::vector<const structure::Data*>( 1, & outputData );
	}

	R operator()( Args... args ) const
	{
		return function( args... );
	}

private:
	FunctionType function;
	std::vector<structure::Data> inputData;
	structure::Data outputData;

	template<unsigned int... indices>
	std::vector<structure::Data>
	static createInputData( FunctionType function,
	                        util::integer_sequence<indices...> )
	{
		using structure::Data;

		return std::vector<Data>{
		    { traits::codeTaskInputDataName<indices>( function ), typeid(Args) }...
		};
	}
};


template<typename Function>
CodeTask<typename std::decay<Function>::type>
codeTask( Function&& function )
{
	using DecayedFunction = typename std::decay<Function>::type;

	return CodeTask<DecayedFunction>( std::forward<Function>(function) );
}


}  // namespace structure
}  // namespace cppwfms


#endif  // CPPWFMS_STRUCTURE_CODETASK_HPP
