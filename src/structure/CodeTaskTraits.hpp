#ifndef CPPWFMS_CODETASKTRAITS_HPP
#define CPPWFMS_CODETASKTRAITS_HPP


#include <cstdint>

#include <string>


namespace cppwfms {
namespace traits {


template<typename T>
struct CodeTaskTraits;


/**
 * Define os identificadores dos parâmetros de entrada e saída de uma CodeTask.
 *
 * Essa implementação padrão define os identificadores como a posição do
 * parâmetro da função, de `0` a `function_traits<T>::arity-1`.
 *
 * Especializações de CodeTaskDataId podem ser providas para gerar
 * identificadores mais adequados para instâncias específicas de CodeTask.
 *
 * @see codeTaskDataId()
 */
template<typename R, typename... Args>
struct CodeTaskTraits<R (Args...)>
{
private:
	using ArgsTypes = std::tuple<Args...>;
	using RetTypes = std::tuple<R>;

public:
	static std::string taskName( R (*function)(Args...) )
	{
		return util::demangleTypeName( typeid(R (Args...)) )
		        + " [" + std::to_string((std::intptr_t)function) + ']';
	}

	template<unsigned int i>
	static std::string inputDataName()
	{
		return util::demangleTypeName( typeid(typename std::tuple_element<i, ArgsTypes>::type) );
	}

	template<unsigned int i>
	static std::string outputDataName()
	{
		return util::demangleTypeName( typeid(typename std::tuple_element<i, RetTypes>::type) );
	}
};


/**
 * Função auxiliar para obter o identificador do parâmetro `i` de `CodeTask`.
 * @see CodeTaskDataId
 */
template<typename R, typename... Args>
std::string codeTaskName( R (*function)(Args...) )
{
	return CodeTaskTraits<R (Args...)>::taskName( function );
}


/**
 * Função auxiliar para obter o identificador do parâmetro `i` de `CodeTask`.
 * @see CodeTaskDataId
 */
template<unsigned int i, typename R, typename... Args>
std::string codeTaskInputDataName( R (*)(Args...) )
{
	return CodeTaskTraits<R (Args...)>::template inputDataName<i>( );
}


/**
 * Função auxiliar para obter o identificador do parâmetro `i` de `CodeTask`.
 * @see CodeTaskDataId
 */
template<unsigned int i, typename R, typename... Args>
std::string codeTaskOutputDataName( R (*)(Args...) )
{
	return CodeTaskTraits<R (Args...)>::template outputDataName<i>( );
}


}  // namespace traits
}  // namespace cppwfms


#endif  // CPPWFMS_CODETASKTRAITS_HPP
