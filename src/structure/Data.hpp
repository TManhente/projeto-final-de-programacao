#ifndef CPPWFMS_STRUCTURE_DATA_HPP
#define CPPWFMS_STRUCTURE_DATA_HPP


#include "WorkflowExceptions.hpp"

#include <string>
#include <memory>
#include <typeinfo>


namespace cppwfms {
namespace structure {


/**
 * Define as informações básicas dos parâmetros de entrada e saída das tarefas e
 * do _workflow_.
 *
 * @see Task, DataValue
 */
class Data
{
public:
	Data( std::string name,
	      const std::type_info & type ) :
	    name( std::move(name) ),
	    type( & type )
	{ }

	std::string getName() const { return name; }
	const std::type_info & getType() const { return *type; }

private:
	std::string name;
	const std::type_info * type;
};


/**
 * Representa um dado do workflow, seja um parâmetro fixo ou saída de uma
 * tarefa.
 *
 * Essa classe representa uma Posição na PetriNet interna do Workflow.
 * Instâncias de DataValue são criadas por Workflow nos métodos addDataValue() e
 * addTask().
 *
 * Workflow, na verdade, cria objetos SpecificDataValue, que é um _template_.
 * DataValue, assim, é apenas uma base comum de forma a permitir que Workflow
 * guarde ponteiros genéricos para as instâncias SpecificDataValue sem ser uma
 * classe _template_ em si.
 *
 * @see Task
 */
class DataValue
{
public:
	virtual ~DataValue() = 0;

	// TODO: As funções e atributos de SpecificDataValue não dependentes do
	//       parâmetro template talvez possam ser movidos pra cá. Será que isso
	//       é necessário ou interessante?
	// TODO: DataValue possui atributos repetidos de Data. Talvez seja
	//       interessante DataValue referenciar Data.
};

// Força DataValue a ser uma classe abstrata, seguindo o padrão descrito no
// livro Effective C++.
DataValue::~DataValue()
{ }


/**
 * Versão concreta de DataValue
 */
template<typename T>
class SpecificDataValue : public DataValue
{
public:
	SpecificDataValue( std::string name ) :
	    name( std::move(name) )
	{ }

	SpecificDataValue( std::string name, T value ) :
	    name( std::move(name) ),
	    value( std::make_shared<T>( std::move(value) ) )
	{ }

	std::string getName() const { return name; }
	const std::type_info & getType() const { return typeid(T); }

	/**
	 * Consulta o valor do dado sem consumí-lo.
	 * @post `hasValue() == true`
	 * @throw NoValue - se não houver valor definido para o dado
	 * @see takeValue()
	 */
	const T & getValue() const
	{
		if ( ! this->hasValue() )
			throw NoValue( this->getName() );

		return * value;
	}

	/**
	 * Consome o valor do dado.
	 * @post `hasValue() == false`
	 * @throw NoValue - se não houver valor definido para o dado
	 * @see getValue()
	 */
	T takeValue()
	{
		if ( ! this->hasValue() )
			throw NoValue( this->getName() );

		std::shared_ptr<T> value;
		std::swap( value, this->value );

		return std::move( * value );
	}

	/**
	 * Define um valor para o dado.
	 * @post `hasValue() == true`
	 * @throw OverridingValue - se já houver um valor definido para o dado
	 */
	void putValue( T value )
	{
		if ( this->hasValue() )
			throw OverridingValue( this->getName() );

		this->value = std::make_shared<T>( std::move(value) );
	}

	bool hasValue() const { return (bool)value; }

private:
	std::string name;
	std::shared_ptr<T> value;

};


}  // namespace structure
}  // namespace cppwfms


#endif  // CPPWFMS_STRUCTURE_DATA_HPP
