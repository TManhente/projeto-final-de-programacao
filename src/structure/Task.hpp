#ifndef CPPWFMS_STRUCTURE_TASK_HPP
#define CPPWFMS_STRUCTURE_TASK_HPP


#include "Data.hpp"

#include <vector>


namespace cppwfms {
namespace structure {


/**
 * Representa uma etapa ou operação do Workflow.
 *
 * @see CodeTask
 */
class Task
{
public:
	virtual ~Task() = 0;
	virtual std::string getName() const = 0;
	virtual std::vector<const Data*> getInputData() const = 0;
	virtual std::vector<const Data*> getOutputData() const = 0;
};

// Força Task a ser uma classe abstrata, seguindo o padrão descrito no livro
// Effective C++.
Task::~Task()
{ }


}  // namespace structure
}  // namespace cppwfms


#endif  // CPPWFMS_STRUCTURE_TASK_HPP
