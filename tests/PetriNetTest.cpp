#define PETRINET_TEST

#include "petriNet/PetriNet.hpp"
#include "petriNet/PetriNetStdTraits.hpp"

#include <gtest/gtest.h>

#include <functional>

#include <deque>


namespace petriNet {
namespace test {


TEST( PetriNetTest, GraphVertexRefInteroperabilityTest )
{
	using PetriNetType =
	        PetriNet<std::deque<int>, std::function<int (int, int)>>;

	using SpecificPlaceRef = PetriNetType::SpecificPlaceRef<std::deque<int>>;
	using ConstSpecificPlaceRef = PetriNetType::SpecificPlaceRef<const std::deque<int>>;

	PetriNetType net;

	const SpecificPlaceRef specificPlaceRef = net.addPlace( );
	const ConstSpecificPlaceRef constSpecificPlaceRef = specificPlaceRef;

	EXPECT_EQ( specificPlaceRef, constSpecificPlaceRef );

	const SpecificPlaceRef specificPlaceRef2 = net.addPlace( );

	EXPECT_NE( specificPlaceRef, specificPlaceRef2 );
	EXPECT_NE( constSpecificPlaceRef, specificPlaceRef2 );


	PetriNetType net2;

	const SpecificPlaceRef net2Place1 = net2.addPlace();
	EXPECT_NE( specificPlaceRef, net2Place1 );  // Mesmo índice, mas grafos diferentes
}


TEST( PetriNetTest, FireTransitionTest )
{
	// A = 5, B = 9, C = A + B, D = 3, E = C - D
	const int aValue = 5, bValue = 9, dValue = 3;
	const int expected = aValue + bValue - dValue;

	PetriNet<std::deque<int>, std::function<int (int, int)>> colouredNet;

#if 0  // Futuras possibilidades:
	// TODO: Rever se esse conceito de inicializar a marcação é interessante,
	//       ou se é mais legal separar completamente estrutura da marcação/execução
	const auto a = colouredNet.addPlace( Token( aValue ) );
	const auto b = colouredNet.addPlace( Token( bValue ) );
	const auto c = colouredNet.addPlace( );
	const auto d = colouredNet.addPlace( Token( dValue ) );
	const auto e = colouredNet.addPlace( );

	// TODO: Ver se dá pra fazer uma versão auxiliar de addTransition() para
	//       receber initializer_lists.
	colouredNet.addTransition( std::plus<int>(), {a, b}, {c} );
	colouredNet.addTransition( std::minus<int>(), {c, d}, {e} );
#endif

	const auto a = colouredNet.addPlace( );
	const auto b = colouredNet.addPlace( );
	const auto c = colouredNet.addPlace( );
	const auto d = colouredNet.addPlace( );
	const auto e = colouredNet.addPlace( );

	colouredNet.addToken( a, aValue );
	colouredNet.addToken( b, bValue );
	colouredNet.addToken( d, dValue );

	auto t1 = colouredNet.addTransition( std::plus<int>(),
	                                     std::make_tuple( a, b ),
	                                     std::make_tuple( c ) );
	auto t2 = colouredNet.addTransition( std::minus<int>(),
	                                     std::make_tuple( c, d ),
	                                     std::make_tuple( e ) );

	// TODO: Rever o trecho abaixo em relação a propriedades transacionais de fireTransition().
#ifdef __clang__
	/* A chamada de fireTransition() abaixo SEM GARANTIA DE SEMÂNTICA
	 * TRANSACIONAL só é válida se a avaliação de argumentos de função for da
	 * esquerda para a direita. Assim, ao tentar extrair um token de `c`, logo
	 * falhará e deixará `d` intocado.
	 * Caso a avaliação fosse da direita para a esquerda, `getToken()` seria
	 * chamado para `d` antes de `c`, o que extrairia o token `3` dessa posição.
	 * Sem a semântica transacional, esse token não é devolvido à posição quando
	 * `getToken(c)` é chamado na sequência e falha. O token, assim, é perdido
	 * e próximas chamadas a fireTransition(t2), mesmo após fireTransition(t1),
	 * sempre falhem.
	 * O padrão C++ não garante nenhuma ordem de avaliação de argumentos. O
	 * Clang é o único compilador conhecido que parece garantir essa avaliação.
	 * http://pimiddy.wordpress.com/2012/10/28/order-of-parameter-evaluation-some-pitfalls/
	 */
	EXPECT_FALSE( colouredNet.fireTransition( t2 ) );
#elif defined(_MSC_VER)
#pragma message( "warning: Assumindo compilador com avaliação de argumentos "  \
                 "da direita para a esquerda, desabilitando trechos do teste " \
                 "por falta de semântica transacional em fireTransition()" )
#else
/* GCC e compatíveis tem #warning. Demais compiladores devem reclamar de
 * diretiva desconhecida.  */
#warning Assumindo compilador com avaliação de argumentos da direita para a  \
         esquerda, desabilitando trechos do teste por falta de semântica     \
         transacional em fireTransition
#endif

	EXPECT_TRUE( colouredNet.fireTransition( t1 ) );
	EXPECT_TRUE( colouredNet.fireTransition( t2 ) );
	EXPECT_EQ( expected, colouredNet.removeToken( e ) );
//	colouredNet.execute( );

//	resultTokens = e->getTokens();
//	const auto resultTokens = *e;
//	EXPECT_EQ( 1, resultTokens.size() );
//	EXPECT_EQ( expected, *resultTokens.begin() );

//	EXPECT_TRUE( colouredNet.enabledTransitions().empty() );
}


TEST(PetriNetTest, ExecuteTest)
{
	/* A = 3   B = 2         C = 7   D = 1
	 *      \ /                   \ /
	 *       x                     +
	 *       |                     |
	 *    R1 = 6 ------\ /----- R2 = 8
	 *                  -
	 *               R3 = -2   E = -4
	 *                      \ /
	 *                       +
	 *                    RF = -6
	 */

	const int aValue = 3, bValue = 2, cValue= 7, dValue = 1, eValue = -4;
	const int rfValue = -6;

	PetriNet<std::deque<int>, std::function<int (int, int)>> net;

	const auto A = net.addPlace( );
	const auto B = net.addPlace( );
	const auto C = net.addPlace( );
	const auto D = net.addPlace( );
	const auto E = net.addPlace( );

	const auto R1 = net.addPlace( );
	const auto R2 = net.addPlace( );
	const auto R3 = net.addPlace( );
	const auto RF = net.addPlace( );

	/* TODO: Aprimorar o teste para aleatorizar a ordem de criação das Transições.
	 *       A lógica de execute() percorre as Transições na ordem em que são
	 *       inseridas. Logo, o teste atual testa apenas o melhor caminho, em
	 *       que todas as Transições, no momento em que o execute() as avalia,
	 *       estão habilitadas, só fazendo uma passagem no loop principal.  */
	const auto T1 = net.addTransition( std::multiplies<int>(),
	                                   std::make_tuple( A, B ),
	                                   std::make_tuple( R1 ) );
	const auto T2 = net.addTransition( std::plus<int>(),
	                                   std::make_tuple( C, D ),
	                                   std::make_tuple( R2 ) );
	const auto T3 = net.addTransition( std::minus<int>(),
	                                   std::make_tuple( R1, R2 ),
	                                   std::make_tuple( R3 ) );
	const auto T4 = net.addTransition( std::plus<int>(),
	                                   std::make_tuple( R3, E ),
	                                   std::make_tuple( RF ) );

	net.addToken( A, aValue );
	net.addToken( B, bValue );
	net.addToken( C, cValue );
	net.addToken( D, dValue );
	net.addToken( E, eValue );

	net.execute();

	EXPECT_TRUE( A->empty() );
	EXPECT_TRUE( B->empty() );
	EXPECT_TRUE( C->empty() );
	EXPECT_TRUE( D->empty() );
	EXPECT_TRUE( E->empty() );

	EXPECT_TRUE( R1->empty() );
	EXPECT_TRUE( R2->empty() );
	EXPECT_TRUE( R3->empty() );

	EXPECT_FALSE( net.isEnabled( T1 ) );
	EXPECT_FALSE( net.isEnabled( T2 ) );
	EXPECT_FALSE( net.isEnabled( T3 ) );
	EXPECT_FALSE( net.isEnabled( T4 ) );

	EXPECT_FALSE( RF->empty() );
	EXPECT_EQ( rfValue, net.removeToken( RF ) );
}


}  // namespace test
}  // namespace petriNet
