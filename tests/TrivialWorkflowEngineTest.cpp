#include <gtest/gtest.h>

#include "Workflow.hpp"
#include "composition/CodeTaskComposition.hpp"
#include "structure/CodeTask.hpp"

#include "WellLogMock.hpp"
#include "WellLogFunctionsMock.hpp"
#include "WorkflowPrinter.hpp"

#include "util/DemangleTypeName.hpp"

#include <vector>


using std::make_pair;


namespace cppwfms {

namespace traits {

namespace {

template<unsigned int i>
std::string selectWellLogsCodeTaskDataId( );

template<>
std::string selectWellLogsCodeTaskDataId<0u>( )
{
	return "wellLogName";
}

template<>
std::string selectWellLogsCodeTaskDataId<1u>( )
{
	return "wellGroup";
}

}  // namespace anônimo


template<>
struct CodeTaskTraits< decltype( test::selectWellLogs) >
{
	static std::string taskName( decltype( test::selectWellLogs ) * function )
	{
		assert( function == &test::selectWellLogs );
		return "Select Well Logs";
	}

	template<unsigned int i>
	static std::string inputDataName()
	{
		return selectWellLogsCodeTaskDataId<i>();
	}

	template<unsigned int i>
	static std::string outputDataName()
	{
		static_assert( i == 0,
		        "selectWellLogs() possui apenas um dado de saída." );
		return "wellLogs";
	}
};

}  // namespace composition

namespace test {

namespace {


const double dummy =
#ifdef _MSC_VER
    /* No VC++12, usar `WellLogMock::dummyValue` estava virando `0.0`,
       o que fazia o teste falhar. Não consegui descobrir o porque, e
       aí usa direto quiet_NaN() por enquanto.  */
    std::numeric_limits<double>::quiet_NaN();
#else
    WellLogMock::dummyValue;
#endif

WellMock wells[] = {
    { "PC01",  // No GR Log
      { make_pair(100.0, RockMock::dolostone) },
      { WellLogMock("DT", 100.0, 10.0, { 10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0 }) }
    },
    { "PC02",  // GR log above Zone top
      { make_pair(100.0, RockMock::limestone) },
      { WellLogMock("DT", 120.0, 5.0, { 100.0, 200.0, 300.0 }),
        WellLogMock("GR", 100.0, 10.0, { 10.0, 20.0, 30.0 }) }
    },
    { "PC03",  // GR log bellow Zone bottom
      { make_pair(110.0, RockMock::siltstone) },
      { WellLogMock("GR", 170.0, 10.0, { 80.0, 90.0, 100.0 }) }
    },
    { "PC04",  // GR log without litologies in Zone
      { make_pair(100.0, RockMock::dolostone),
        make_pair(140.0, RockMock::siltstone),
        make_pair(180.0, RockMock::mudstone) },
      { WellLogMock("GR", 100.0, 10.0, { 10.0, dummy, 30.0, 40.0, dummy, 60.0, dummy, 80.0, dummy, 100.0 }) }
    },
    { "PC05",  // GR log without minimum percentage
      { make_pair(100.0, RockMock::dolostone),
        make_pair(150.0, RockMock::mudstone) },
      { WellLogMock("GR", 100.0, 10.0, { 10.0, dummy, 30.0, dummy, dummy, 60.0, 70.0, 80.0, 90.0, 100.0 }) }
    },
    { "PC06",  // Valid with mean value of 55.0
      { make_pair(100.0, RockMock::dolostone),
        make_pair(160.0, RockMock::mudstone) },
      { WellLogMock("GR", 100.0, 10.0, { 10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0 }) }
    },
    { "PC07",  // Valid with mean value of 110.0
      { make_pair(100.0, RockMock::dolostone),
        make_pair(140.0, RockMock::mudstone) },
      { WellLogMock("GR", 100.0, 10.0, { 20.0, 40.0, 60.0, 80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0 }) }
    },
    { "PC08",  // Valid with mean value of 82.0
      { make_pair(100.0, RockMock::dolostone),
        make_pair(170.0, RockMock::mudstone) },
      { WellLogMock("GR", 100.0, 10.0, { 13.0, 480.0, 32.0, 34.0, 78.0, 168.0, 48.0, 26.0, 48.0, 593.0 }) }
    }
};


WellGroupMock createTestData( )
{
	WellGroupMock wellGroupMock( "GrupoTeste");
	for ( unsigned int i = 0; i < sizeof(wells)/sizeof(wells[0]); ++i ) {
		// wellGroupMock.addWell( new WellMock( wells[i] ) );  // FIXME: Vazamento de memória!!!
		wellGroupMock.addWell( & wells[i] );
	}

	return wellGroupMock;
}

}  // namespace anônimo


TEST(TrivialWorkflowEngineUnitTests, CodeTasksAsBindTest)
{
	using composition::codeTask;

	const WellGroupMock wellGroupMock = createTestData( );

	const auto selectWellLogsTask =
	        codeTask( selectWellLogs, "GR", wellGroupMock );

	const auto res1 = selectWellLogsTask.evaluate( );
	EXPECT_EQ( 7, res1.size() );
	for ( auto it = res1.begin(); it != res1.end(); ++it )
		EXPECT_EQ( "GR", (*it)->getName() );


	const auto filterByZoneTask =
	        codeTask( filterLogsByZone,
	                  selectWellLogsTask,
	                  ZoneMock( 130.0, 160.0 )
	                  );

	filterByZoneTask.evaluate( );


	const std::set<RockMock> rocks = { RockMock::dolostone,
	                                   RockMock::mudstone };

	const auto completeWorkflow =
	        codeTask( calculateLogsArithmeticMean,
	                  codeTask( selectLogsByMinimumPercentage,
	                            codeTask( filterLogsByLithology,
	                                      filterByZoneTask,
	                                      rocks
	                                      ),
	                            0.8
	                            )
	                  );

	const auto res2 = completeWorkflow.evaluate( );

	EXPECT_EQ( 3, res2.size() );

	const std::map<std::string, double> expectedMeans = {
	    make_pair( "PC06", 55.0 ),
	    make_pair( "PC07", 110.0 ),
	    make_pair( "PC08", 82.0 )
	};

	for ( auto it = res2.begin(); it != res2.end(); ++it )
	{
		const WellAttributeMock & wellAttribute = (*it);
		const double expectedMean = expectedMeans.at( wellAttribute.getWell().getName() );
		EXPECT_EQ( expectedMean, wellAttribute.getValue() );
	}
}


TEST(TrivialWorkflowEngineUnitTests, CodeTaskNamesTest)
{
	using composition::codeTask;

	const WellGroupMock wellGroupMock = createTestData( );

	const auto selectWellLogsTask =
	        codeTask( selectWellLogs, "GR", wellGroupMock );

	const auto filterByZoneTask =
	        codeTask( filterLogsByZone,
	                  selectWellLogsTask,
	                  ZoneMock( 130.0, 160.0 )
	                  );

	const std::set<RockMock> rocks = { RockMock::dolostone,
	                                   RockMock::mudstone };

	const auto completeWorkflow =
	        codeTask( calculateLogsArithmeticMean,
	                  codeTask( selectLogsByMinimumPercentage,
	                            codeTask( filterLogsByLithology,
	                                      filterByZoneTask,
	                                      rocks
	                                      ),
	                            0.8
	                            )
	                  );

	std::cout << "--- Workflow of single task ---" << std::endl;
	util::printWorkflow( std::cout, selectWellLogsTask );
	std::cout << std::endl;

	std::cout << "--- Workflow of two tasks ---" << std::endl;
	util::printWorkflow( std::cout, filterByZoneTask );
	std::cout << std::endl;

	std::cout << "--- Workflow of various tasks ---" << std::endl;
	util::printWorkflow( std::cout, completeWorkflow );
	std::cout << std::endl;


	std::cout << std::endl << "--- selectWellLogsTask data ---" << std::endl;
	std::cout << "Task name: " << selectWellLogsTask.getTask().getName() << '\n'
	          << "Return: " << selectWellLogsTask.getTask().getOutputData().front()->getName() << std::endl;
	std::cout << "Inputs:" << std::endl;
	for ( const cppwfms::structure::Data * data : selectWellLogsTask.getTask().getInputData() )
		std::cout << "- " << data->getName() << '\t' << ::util::demangleTypeName( data->getType() ) << std::endl;

	std::cout << std::endl << "--- filterByZoneTask data ---" << std::endl;
	std::cout << "Task name: " << filterByZoneTask.getTask().getName() << '\n'
	          << "Return: " << filterByZoneTask.getTask().getOutputData().front()->getName() << std::endl;
	std::cout << "Inputs:" << std::endl;
	for ( const cppwfms::structure::Data * data : filterByZoneTask.getTask().getInputData() )
		std::cout << "- " << data->getName() << '\t' << ::util::demangleTypeName( data->getType() ) << std::endl;

	std::cout << std::endl << "--- completeWorkflow data ---" << std::endl;
	std::cout << "Task name: " << completeWorkflow.getTask().getName() << '\n'
	          << "Return: " << completeWorkflow.getTask().getOutputData().front()->getName() << std::endl;
	std::cout << "Inputs:" << std::endl;
	for ( const cppwfms::structure::Data * data : completeWorkflow.getTask().getInputData() )
		std::cout << "- " << data->getName() << '\t' << ::util::demangleTypeName( data->getType() ) << std::endl;

	// TODO: Substituir impressões por EXPECT_EQ().
}


TEST(TrivialWorkflowEngineUnitTests, WorkflowTest)
{
	using cppwfms::structure::codeTask;

	const WellGroupMock wellGroup = createTestData();
	const char * const logGroup = "GR";

	cppwfms::Workflow wf;
	const auto logGroupRef = wf.addDataValue( "logGroup", logGroup );
	const auto wellGroupRef = wf.addDataValue( "wellGroup", wellGroup );
	const auto selectWellLogsRes =
	        wf.addTask( codeTask( selectWellLogs ),
	                    std::make_tuple( logGroupRef, wellGroupRef ) );

	const auto resultRef = std::get<1>( selectWellLogsRes );

	EXPECT_TRUE( logGroupRef->hasValue() );
	EXPECT_EQ( logGroup, logGroupRef->getValue() );

	EXPECT_TRUE( wellGroupRef->hasValue() );
	EXPECT_EQ( wellGroup.size(), wellGroupRef->getValue().size() );

	EXPECT_FALSE( resultRef->hasValue() );

	EXPECT_NO_THROW( wf.execute() );

	ASSERT_TRUE( resultRef->hasValue() );
	EXPECT_EQ( 7, resultRef->takeValue().size() );


	// TODO: Não era pra fazer esses putValue(), falta uma função de reset() do
	//       workflow:
	logGroupRef->putValue( logGroup );
	wellGroupRef->putValue( wellGroup );


	const auto zoneRef = wf.addDataValue( "zone", ZoneMock( 130.0, 160.0 ) );
	const auto filterByZoneRes =
	        wf.addTask( codeTask( filterLogsByZone ),
	                    std::make_tuple( std::get<1>(selectWellLogsRes),
	                                     zoneRef ) );

	const auto rocksRef =
	        wf.addDataValue( "lithology",
	                         std::set<RockMock>{ RockMock::dolostone,
	                                             RockMock::mudstone } );
	const auto filterByLithologyRes =
	        wf.addTask( codeTask( filterLogsByLithology ),
	                    std::make_tuple( std::get<1>(filterByZoneRes),
	                                     rocksRef ) );

	const auto minimumPercentRef = wf.addDataValue( "minimumPercentage", 0.8 );
	const auto selectByMinimumPercentageRes =
	        wf.addTask( codeTask( selectLogsByMinimumPercentage ),
	                    std::make_tuple( std::get<1>(filterByLithologyRes),
	                                     minimumPercentRef ) );

	const auto calculateArithmeticMeanRes =
	        wf.addTask( codeTask(calculateLogsArithmeticMean),
	                    std::make_tuple( std::get<1>(selectByMinimumPercentageRes) ) );

	wf.execute();

	const auto result2Ref = std::get<1>( calculateArithmeticMeanRes );
	EXPECT_TRUE( result2Ref->hasValue() );

	const auto res2 = result2Ref->getValue();

	EXPECT_EQ( 3, res2.size() );

	const std::map<std::string, double> expectedMeans = {
	    make_pair( "PC06", 55.0 ),
	    make_pair( "PC07", 110.0 ),
	    make_pair( "PC08", 82.0 )
	};

	for ( auto it = res2.begin(); it != res2.end(); ++it )
	{
		const WellAttributeMock & wellAttribute = (*it);
		const double expectedMean = expectedMeans.at( wellAttribute.getWell().getName() );
		EXPECT_EQ( expectedMean, wellAttribute.getValue() );
	}
}

// TODO: Documentar:
// composeWorkflow() :
// Retorna uma instância de workflow (workflow execuável, padronizar
// nomenclatura) nos casos em que as tarefas permitam isso (isto é, não tem
// parâmetro em aberto nem tarefas ou dados abstratos, unbounded).
// Retorna um template de workflow (workflow abstrato, padronizar
// nomenclatura) nos demais casos. O grau de abstração ou especialização do
// workflow retornado dependem da abstração ou especialização das tarefas e
// dados passados. (Será preciso fazer um serviço de especialização de
// workflows mais abstratos). Engines podem ter métodos para a execução
// desses templates de workflow caso sejam passados também os parâmetros
// faltando e/ou as especializações faltando (ou algum algoritmo para fazer
// os bounds faltando).
//	const auto workflow =
//	        composeWorkflow(
//	            selectWellLogs( _1("logGroup"), _2("wellGroup") )
//	            );

//	workflow.execute( "GR", wellGroupMock );
//	workflow.execute( "DT", wellGroupMock );

//	engine.execute( workflow, {
//	{ "wellGroup", wellGroupMock },
//	{ "logGroup", "GR" }
//	} );

//	engine.execute( workflow, {
//	{ "wellGroup", wellGroupMock },
//	{ "logGroup", "DT" }
//	} );

//	workflow =
//	composeWorkflow(
//		Task<mediaAritmetica>(
//			Task<filtroPercentualMinimo>(
//				Task<filtroLitologia>(
//					Task<filtroZona>(
//						Task<selecionaPerfis( Param<GrupoPerfis>, Param<GrupoPocos> ),
//						z01
//					),
//					{...}
//				),
//				80%
//			)
//		)
//	);


#if 0  // TODO: Fazer esse teste
TEST(TrivialWorkflowEnginePerformancenTests, StdBindPerformance)
{
	using std::bind;
	using placeholders = std::placeholders;

	const auto bound =
	        bind( mediaAritmetica,
	              bind( filtroPercentualMinimo,
	                    bind( filtroLitologia,
	                          bind( filtroZona,
	                                bind( selecionaPerfils,
	                                      placeholders::_1,
	                                      placeholders::_2
	                                ),
	                                z01
	                          ),
	                          litologias
	                    ),
	                    0.8 )
	              );

	// TODO: Cronometrar
	bound( gr, grupoPocoss );  // TODO: Se passar parâmetros estiver muito
	                           //       difícil, fazer primeiro sem
	                           //       placeholders (workflow concreto).
}
#endif


}  // namespace test
}  // namespace cppwfms
