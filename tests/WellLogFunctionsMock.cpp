#include "WellLogFunctionsMock.hpp"

#include "WellLogMock.hpp"

#include <cassert>
#include <cmath>

#include <algorithm>
#include <functional>


using std::cref;
using std::find_if;
using std::ptr_fun;
using std::set;
using std::string;
using std::vector;


namespace cppwfms {
namespace test {


namespace {

bool checkWellLogName( const WellLogMock * const wellLog, const string & name )
{
	return wellLog->getName() == name;
}

}  // namespace anônimo


vector<const WellLogMock*> selectWellLogs( const string & name,
                                           const WellGroupMock & wellGroup )
{
	vector<const WellLogMock*> selectedWellLogs;

	for( auto it = wellGroup.begin(); it != wellGroup.end(); ++it )
	{
		const WellMock * const well = (*it);
		assert( well );

		const auto wellLogs = well->getLogs();

		const auto itFound =
		        find_if( wellLogs.begin(),
		                 wellLogs.end(),
		                 bind( checkWellLogName,
		                       std::placeholders::_1,
		                       cref( name ) )
		                 );

		if ( itFound != wellLogs.end() )
			selectedWellLogs.push_back( *itFound );
	}

	return selectedWellLogs;
}


WellLogMock filterLogByZone( WellLogMock wellLog,
                             const ZoneMock & zone )
{
	// Validações rápidas:
	const double
	        wellLogBottom = wellLog.getBottomDepth( ),
	        wellLogTop = wellLog.getTopDepth( ),
	        zoneBottom = zone.getBottom( ),
	        zoneTop = zone.getTop( );

	if ( wellLogBottom < zoneTop || zoneBottom < wellLogTop )
		return WellLogMock( wellLog.getName(), zone.getTop(), 0.0, std::vector<double>(), & wellLog.getWell() );  // Well Log ouside of Zone
	if ( zoneTop <= wellLogTop && wellLogBottom <= zoneBottom )
		return wellLog;  // Well Log is entirelly inside Zone

	// Well Log is partialy inside, partialy outside the Zone:

	const double firstSample = std::max( wellLogTop, zoneTop );
	const double zoneTopSample = ( firstSample - wellLogTop ) /
	                             wellLog.getSampling( );
	const size_t roundedZoneTopIndex = (size_t)std::ceil( zoneTopSample );
	assert( roundedZoneTopIndex < wellLog.size() );

	const double lastSample = std::min( wellLogBottom, zoneBottom );
	const double zoneBottomSample = ( lastSample - wellLogTop ) /
	                                wellLog.getSampling( );
	const size_t roundedZoneBottomIndex = (size_t)std::ceil( zoneBottomSample );
	assert( roundedZoneBottomIndex < wellLog.size() );

#if 0  // Versão sem alterar a geometria
	vector<double> values( wellLog.size(), WellLogMock::dummyValue );
	std::copy( wellLog.begin() + roundedZoneTopIndex,
	           wellLog.begin() + roundedZoneBottomIndex + 1,  // After the last
	           values.begin() + roundedZoneTopIndex );
	const double newTopDepth = wellLog.getTopDepth();
#else  // Versão alterando a geometria
	vector<double> values( wellLog.begin() + roundedZoneTopIndex,
	                       wellLog.begin() + roundedZoneBottomIndex + 1 );
	const double newTopDepth = firstSample;
#endif

	return WellLogMock( wellLog.getName(),
	                    newTopDepth,
	                    wellLog.getSampling(),
	                    std::move(values),
	                    & wellLog.getWell() );
}


WellLogMock filterLogByLithology( const WellLogMock & wellLog,
                                  const set<RockMock> & rocks)
{
	vector<double> values( wellLog.size(), WellLogMock::dummyValue );

	const LithologyMock & wellLithology = wellLog.getWell().getLithology();

	size_t i = 0;
	double depth = wellLog.getTopDepth();
	for ( auto it = wellLog.begin();
	      it != wellLog.end();
	      ++it, ++i, depth += wellLog.getSampling() )
	{
		const auto rockIt = --wellLithology.upper_bound( depth );
		assert( rockIt != wellLithology.end() );

		const RockMock & rock = rockIt->second;
		if ( rocks.find( rock ) != rocks.end( ) )
			values.at( i ) = *it;
	}

	return WellLogMock( wellLog.getName(),
	                    wellLog.getTopDepth(),
	                    wellLog.getSampling(),
	                    values,
	                    & wellLog.getWell() );
}



vector<WellLogMock> filterLogsByZone(
        const vector<const WellLogMock *> & wellLogs,  // TODO: Deveria ser std::vector<WellLogMock> p/ move semantics
        const ZoneMock & zone )
{
	vector<WellLogMock> results;
	results.reserve( wellLogs.size( ) );

	for( auto it = wellLogs.begin( ); it != wellLogs.end( ); ++it )
		results.push_back( filterLogByZone( (**it), zone ) );

	return results;
}


vector<WellLogMock> filterLogsByLithology(
        const vector<WellLogMock> & wellLogs,  // TODO: Deveria ser std::vector<WellLogMock> p/ move semantics
        const set<RockMock> & rocks )
{
	vector<WellLogMock> results;
	results.reserve( wellLogs.size( ) );

	for( auto it = wellLogs.begin( ); it != wellLogs.end( ); ++it )
		results.push_back( filterLogByLithology( (*it), rocks ) );

	return results;
}


namespace {

bool isValidValue( double value )
{
	return std::isfinite( value );  // Exclui NaN e ∞
}

}  // namespace anônimo


vector<WellLogMock>
selectLogsByMinimumPercentage( const vector<WellLogMock> & wellLogs,
                               double minimumPercentage )
{
	vector<WellLogMock> result;
	result.reserve( wellLogs.size() );

	for ( auto it = wellLogs.begin(); it != wellLogs.end(); ++it )
	{
		const WellLogMock & wellLog = (*it);
		const size_t wellLogSize = wellLog.size();

		const auto validValues = std::count_if( wellLog.begin(), wellLog.end(), &isValidValue );
		if ( wellLogSize > 0 && validValues / wellLogSize >= minimumPercentage )
			result.push_back( wellLog );
	}

	return result;
}


vector<WellAttributeMock>
calculateLogsArithmeticMean( const vector<WellLogMock> & wellLogs )
{
	vector<WellAttributeMock> result;
	result.reserve( wellLogs.size() );

	for ( auto it = wellLogs.begin(); it != wellLogs.end(); ++it )
	{
		const WellLogMock & wellLog = (*it);

		size_t count = 0;
		double sum = 0;
		for ( auto itValue = wellLog.begin(); itValue != wellLog.end(); ++itValue )
		{
			const double value = (*itValue);
			if ( WellLogMock::isValid( value ) )
			{
				sum += value;
				++count;
			}
		}

		result.push_back( WellAttributeMock(
		                      & wellLog.getWell(),
		                      count > 0 ?
		                          sum/count :
		                          WellLogMock::dummyValue
		                          )
		                  );
	}

	return result;
}


}  // namespace test
}  // namespace cppwfms
