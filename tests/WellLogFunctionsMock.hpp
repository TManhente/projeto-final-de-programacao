#ifndef CPPWFMS_TEST_WELLOGFUNCTIONSMOCK_HPP
#define CPPWFMS_TEST_WELLOGFUNCTIONSMOCK_HPP


#include <set>
#include <string>
#include <vector>


namespace cppwfms {
namespace test {


class WellAttributeMock;
class WellGroupMock;
class WellLogMock;
class WellMock;
class ZoneMock;

enum class RockMock : unsigned short;


std::vector<const WellLogMock*>
selectWellLogs( const std::string & name,
                const WellGroupMock & wellGroup );

WellLogMock filterLogByZone( WellLogMock wellLog,
                             const ZoneMock & zone );

WellLogMock filterLogByLithology( const WellLogMock & wellLog,
                                  const std::set<RockMock> & rocks );

std::vector<WellLogMock*>
selectByMinimumPercentage( const std::vector<WellLogMock*> & wellLogs,
                           double minimumPercentage );

std::vector<WellAttributeMock>
calculateArithmeticMean( const std::vector<WellLogMock*> & wellLogs );


// TODO: Rever essas versões em Grupo de Poços (modelar como composição de
//       operação foreach?) e de tipos dos parâmetros de entrada.
std::vector<WellLogMock> filterLogsByZone(
        const std::vector<const WellLogMock*> & wellLogs,
        const ZoneMock & zone );

std::vector<WellLogMock> filterLogsByLithology(
        const std::vector<WellLogMock> & wellLog,
        const std::set<RockMock> & rocks );

std::vector<WellLogMock>
selectLogsByMinimumPercentage( const std::vector<WellLogMock> & wellLogs,
                               double minimumPercentage );

std::vector<WellAttributeMock>
calculateLogsArithmeticMean( const std::vector<WellLogMock> & wellLogs );


}  // namespace test
}  // namespace cppwfms


#endif // CPPWFMS_TEST_WELLOGFUNCTIONSMOCK_HPP
