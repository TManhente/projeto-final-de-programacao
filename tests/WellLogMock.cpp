#include "WellLogMock.hpp"

#include <boost/iterator/transform_iterator.hpp>

#include <cassert>
#include <cmath>

#include <algorithm>
#include <iostream>


using std::vector;


namespace cppwfms {
namespace test {

ZoneMock::ZoneMock(double top , double bottom) :
    top( top ),
    bottom( bottom )
{
	assert( top <= bottom );  // O referencial é invertido (como se fossem números negativos)
}

WellAttributeMock::WellAttributeMock( const WellMock * well, double value ) :
    well( well ),
    value( value )
{
	assert(well);
}


const double WellLogMock::dummyValue = std::numeric_limits<double>::quiet_NaN();

bool WellLogMock::isValid( double value )
{
	return std::isfinite( value );
}

WellLogMock::WellLogMock( std::string name,
                          double topDepth,
                          double sampling,
                          std::vector<double> values,
                          const WellMock * well ) :
    name( std::move(name) ),
    well( well ),
    topDepth( topDepth ),
    sampling( sampling ),
    values( values )
{
	assert( sampling >= 0.0 );
}

const WellMock & WellLogMock::getWell( ) const
{
	assert( well );
	return * well;
}

WellLogMock::const_iterator WellLogMock::nextValue( double depth ) const
{
	double relativeDepth = topDepth - depth;
	double samples = relativeDepth / sampling;
	int roundedIndex = (int)std::ceil( samples );

	return values.begin() + std::min( (size_t)std::max( roundedIndex, 0 ),
	                                  values.size() );
}

namespace {
template<typename T>
struct AddressOf : public std::unary_function<T&, T*>
{
	T* operator()( T & o ) const { return & o; }
};
}  // namespace anônimo


WellMock::WellMock( std::string name,
          std::initializer_list<std::pair<const double, RockMock>> lithology,
          std::initializer_list<WellLogMock> logs) :
    name( std::move(name) ),
    lithology( std::move( lithology ) )/*,
    logs( logs )*/  // TODO: Rever questão do setWell() (set faz os iteradores serem const, inviabilizando chamadas de setWell() aqui).
{
	for( auto it = logs.begin(); it != logs.end(); ++it )
		this->addLog( *it );
//		it->setWell( this );  // TODO: Rever
}


std::vector<const WellLogMock*> WellMock::getLogs( ) const
{
	using boost::make_transform_iterator;

	return vector<const WellLogMock*>(
	            make_transform_iterator( logs.begin(), AddressOf<const WellLogMock>() ),
	            make_transform_iterator( logs.end(), AddressOf<const WellLogMock>() ) );
}


WellGroupMock::WellGroupMock( std::string name ) :
    name( std::move(name) )
{
}

void WellMock::addLog( WellLogMock log )
{
	log.setWell( this );
	const bool inserted = logs.insert( std::move( log ) ).second;
	assert( inserted );
}

void WellGroupMock::addWell( WellMock * well )
{
	const bool inserted = wells.insert( well ).second;
	assert( inserted );
}


std::ostream& operator<< ( std::ostream & os,
                           const RockMock & rock )
{
	switch( rock )
	{
	case RockMock::dolostone: os << "dolostone"; break;
	case RockMock::limestone: os << "limestone"; break;
	case RockMock::mudstone: os << "mudstone"; break;
	case RockMock::shale: os << "shale"; break;
	case RockMock::siltstone: os << "siltstone"; break;
	}

	return os;
}

std::ostream& operator<< ( std::ostream & os,
                           const std::set<RockMock> & rocks )
{
	os << '{';
	std::string sep;
	for ( const RockMock & rock : rocks )
	{
		os << sep << rock;
		sep = ", ";
	}
	os << '}';

	return os;
}

std::ostream& operator<< ( std::ostream & os,
                           const WellGroupMock & wellGroup )
{
	os << wellGroup.getName();
	return os;
}

std::ostream& operator<< ( std::ostream & os,
                           const ZoneMock & zone )
{
	os << "Zone{" << zone.getTop() << ", " << zone.getBottom() << '}';
	return os;
}


}  // namespace test
}  // namespace cppwfms
