#ifndef CPPWFMS_TEST_WELLLOGMOCK_HPP
#define CPPWFMS_TEST_WELLLOGMOCK_HPP


#include <initializer_list>
#include <iosfwd>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <vector>


namespace cppwfms {
namespace test {


namespace details {

struct CompareNames
{
	template<typename T>
	typename std::enable_if< ! std::is_pointer<T>::value, bool>::type
	operator()( const T & a, const T & b ) const
	{ return a.getName() < b.getName(); }

	template<typename T>
	typename std::enable_if<std::is_pointer<T>::value, bool>::type
	operator()( const T & a, const T & b ) const
	{ return a->getName() < b->getName(); }
};

}  // namespace details


class ZoneMock
{
public:
	ZoneMock( double top, double bottom );

	double getTop( ) const { return top; }
	double getBottom( ) const { return bottom; }

private:
	double top;
	double bottom;
};


class WellMock;


class WellAttributeMock
{
public:
	WellAttributeMock( const WellMock * well, double value );

	const WellMock & getWell( ) const { return * well; }
	double getValue( ) const { return value; }
private:
	std::string name;
	const WellMock * well;
	double value;
};


class WellLogMock
{
public:
	static const double dummyValue;

	static bool isValid( double value );

	WellLogMock( std::string name,
	             double topDepth,
	             double sampling,
	             std::vector<double> values,
	             const WellMock * well = nullptr );

	std::string getName( ) const { return name; }
	const WellMock & getWell( ) const;

	double getSampling( ) const { return sampling; }

	double getTopDepth( ) const { return topDepth; }
	double getBottomDepth( ) const {
		return topDepth + ( sampling * (this->size()-1) );
	}

	typedef std::vector<double>::const_iterator const_iterator;

	const_iterator begin( ) const { return values.begin( ); }
	const_iterator end( ) const { return values.end( ); }

	size_t size( ) const { return values.size( ); }
	bool empty( ) const { return values.empty( ); }

	const_iterator nextValue( double depth ) const;

private:
	std::string name;
	const WellMock * well;
	double topDepth;
	double sampling;
	std::vector<double> values;

	void setWell( const WellMock * well ) { this->well = well; }
	friend class WellMock;

};


enum class RockMock : unsigned short {
	dolostone,
	limestone,
	mudstone,
	shale,
	siltstone
};


typedef std::map<double, RockMock> LithologyMock;


class WellMock
{
public:
	WellMock( std::string name,
	          std::initializer_list<std::pair<const double, RockMock>> lithology,
	          std::initializer_list<WellLogMock> logs);

	std::string getName() const { return name; }

	const LithologyMock & getLithology() const { return lithology; }

	std::vector<const WellLogMock*> getLogs() const;
	void addLog( WellLogMock log );

private:
	std::string name;
	LithologyMock lithology;
	std::set<WellLogMock, details::CompareNames> logs;

	WellMock(const WellMock &) = delete;
	WellMock( WellMock && ) = delete;
};


class WellGroupMock
{
	typedef std::set<WellMock*, details::CompareNames> WellsSet;
public:
	typedef WellsSet::const_iterator const_iterator;

	WellGroupMock( std::string name );

	std::string getName() const { return name; }

	const_iterator begin() const { return wells.begin(); }
	const_iterator end() const { return wells.end(); }

	size_t size() const { return wells.size(); }
	bool empty() const { return wells.empty(); }

	void addWell( WellMock * well );

private:
	std::string name;
	WellsSet wells;
};


std::ostream& operator<< ( std::ostream &, const std::set<RockMock> & );
std::ostream& operator<< ( std::ostream &, const RockMock & );
std::ostream& operator<< ( std::ostream &, const WellGroupMock & );
std::ostream& operator<< ( std::ostream &, const ZoneMock & );


}  // namespace test
}  // namespace cppwfms


#endif  // CPPWFMS_TEST_WELLLOGMOCK_HPP
