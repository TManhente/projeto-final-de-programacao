#ifndef TEST_WORKFLOWPRINTER_HPP
#define TEST_WORKFLOWPRINTER_HPP


#include "composition/CodeTaskComposition.hpp"

#include "util/IntegerSequence.hpp"

#include <iostream>
#include <tuple>


namespace cppwfms {
namespace test {
namespace util {


template<typename T>
struct WorkflowPrinter
{
	static void print( std::ostream & os, const T & value )
	{
		os << value;
	}
};

template<typename F, typename... Args>
struct WorkflowPrinter< composition::CodeTaskInstance<F, Args...> >
{
	static void print( std::ostream & os,
	                   const composition::CodeTaskInstance<F, Args...> & codeTaskInstance)
	{
		os << codeTaskInstance.getTask().getName()
		   << '(';
		printArgs( os,
		           codeTaskInstance.getArguments(),
		           ::util::make_index_sequence<sizeof...(Args)>() );
		os << ')';
	}

private:
	template<unsigned int i, unsigned int j, unsigned int... indices>
	static void printArgs( std::ostream & os,
	                       const std::tuple<Args...> & args,
	                       ::util::integer_sequence<i, j, indices...> /*sequence*/ )
	{
		using ::util::integer_sequence;

		printArgs( os, args, integer_sequence<i>() );
		os << ", ";
		printArgs( os, args, integer_sequence<j, indices...>() );
	}

	template<unsigned int i>
	static void printArgs( std::ostream & os,
	                       const std::tuple<Args...> & args,
	                       ::util::integer_sequence<i> )
	{
		using Type = typename std::tuple_element<i, std::tuple<Args...> >::type;
		WorkflowPrinter<Type>::print( os, std::get<i>( args ) );
	}

	static void printArgs( std::ostream & /*os*/,
	                       const std::tuple<Args...> & /*args*/,
	                       ::util::integer_sequence<> /*indices*/ )
	{ }
};


template<typename T>
void printWorkflow( std::ostream & os, const T & workflow )
{
	WorkflowPrinter<T>::print( os, workflow );
}


}  // namespace util
}  // namespace test
}  // namespace cppwfms


#endif  // TEST_WORKFLOWPRINTER_HPP
