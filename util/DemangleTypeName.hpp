#ifndef UTIL_DEMANGLETYPENAME_HPP
#define UTIL_DEMANGLETYPENAME_HPP


#include <memory>  // Para std::unique_ptr
#include <string>
#include <typeinfo>

#ifdef __GNUC__
#include <cxxabi.h>  // Para abi::__cxa_demangle()
#endif


namespace util {


/**
 * Faz o _demangle_ do nome do tipo retornado por `typeInfo.name()`, quando
 * possível.
 *
 * @return O nome do tipo representado por `typeInfo` na forma _demangled_, ou
 *         `typeInfo.name()` caso o _demangle_ não seja possível ou falhe.
 */
std::string demangleTypeName( const std::type_info & typeInfo )
{
	const char * const typeInfoName = typeInfo.name();

#ifdef __GNUC__
	{
		int status;
		const std::unique_ptr<const char> cxaDemangledName(
				abi::__cxa_demangle( typeInfoName, NULL, NULL, &status )
					);
		if ( cxaDemangledName )
			return std::string( cxaDemangledName.get() );
	}
#endif

	// MSVC: typeInfoName já retorna o nome certo (ao contrário de raw_name()).
	// Outros compiladores: Não tem jeito, tem que retornar typeInfoName mesmo.
	return std::string( typeInfoName );
}


}  // namespace util


#endif  // UTIL_DEMANGLETYPENAME_HPP
