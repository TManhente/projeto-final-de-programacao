#ifndef UTIL_INTEGERSEQUENCE_HPP
#define UTIL_INTEGERSEQUENCE_HPP


/**
 * @file IntegerSequence.hpp
 * Geração de sequências estáticas de inteiros.
 * Retirado de http://stackoverflow.com/questions/17424477/implementation-c14-make-integer-sequence
 * para implementar std::integer_sequence, que só estará disponível no C++14
 */


namespace util {


// Forward-declarations:
template<unsigned...> struct integer_sequence;
template<unsigned N> struct make_index_sequence;


namespace details {

template<class T> using Invoke = typename T::type;

template<class S1, class S2> struct concat;

template<unsigned... I1, unsigned... I2>
struct concat<integer_sequence<I1...>, integer_sequence<I2...>> :
        integer_sequence<I1..., (sizeof...(I1)+I2)...>
{};

template<class S1, class S2>
using Concat = Invoke<concat<S1, S2>>;

template<unsigned N> using GenSeq = Invoke<make_index_sequence<N>>;

}  // namespace details


template<unsigned...>
struct integer_sequence
{
	using type = integer_sequence;
};


template<unsigned N>
struct make_index_sequence :
        details::Concat<details::GenSeq<N/2>, details::GenSeq<N - N/2>>
{};

// Casos base:
template<> struct make_index_sequence<0> : integer_sequence<> {};
template<> struct make_index_sequence<1> : integer_sequence<0> {};


}  // namespace util


#endif  // UTIL_INTEGERSEQUENCE_HPP
