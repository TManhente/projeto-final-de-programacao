#ifndef UTIL_ITERATORHELPER_HPP
#define UTIL_ITERATORHELPER_HPP


/**
 * @file IteratorHelper.hpp
 *
 * Estruturas auxiliares para definir iteradores const e não-const a partir de
 * _templates_.
 *
 * Essas estruturas foram parcialmene inspiradas em
 * http://www.sj-vs.net/c-implementing-const_iterator-and-non-const-iterator-without-code-duplication/.
 */


#include <type_traits>


namespace util {


// Documentação em IteratorHelper.dox
template<typename Type1, typename Type2>
struct IsSameExceptConst :
        std::is_same< typename std::remove_const<Type1>::type,
                      typename std::remove_const<Type2>::type >
{ };

/**
 * Atalho para std::enable_if usando IsSameExceptConst como predicado.
 */
template<typename Type1, typename Type2>
struct EnableIfSameExceptConst :
        std::enable_if< IsSameExceptConst<Type1, Type2>::value >
{ };


// Documentação em IteratorHelper.dox
template<typename NonConstType, typename ConstType>
struct IsNonConstToConst :
        std::integral_constant<
                bool,
                ! std::is_same<ConstType, NonConstType>::value &&
                std::is_same<typename std::remove_const<ConstType>::type,
                             NonConstType>::value
        >
{ };

/**
 * Atalho para std::enable_if usando IsSameExceptConst como predicado.
 */
template<typename NonConstType, typename ConstType>
struct EnableIfNonConstToConst :
        std::enable_if< IsNonConstToConst<NonConstType, ConstType>::value >
{ };


// Documentação em IteratorHelper.dox
template<typename Reference, typename Container>
struct TypeBroker : std::conditional< std::is_const<Reference>::value,
                                      const Container,
                                      Container >
{ };


}  // namespace util


#endif  // UTIL_ITERATORHELPER_HPP
