#ifndef UTIL_MAKERANGE_HPP
#define UTIL_MAKERANGE_HPP


/**
 * @file MakeRange.hpp
 *
 * Provê estruturas para adaptar objtetos que representam _ranges_ para uso em
 * _for-range loops_.
 */


#include <utility>


namespace util {


/**
 * Classe auxiliar que armazena iteradores para o início e o final da _range_.
 *
 * Essa classe provê a interface de _range_ exigida por _for-range loops_.
 *
 * Objetos Range podem ser facilmente construídos usando makeRange().
 */
template<typename Iterator>
class Range
{
public:
	Range( Iterator first, Iterator last ) :
	    first( std::move(first) ),
	    last( std::move(last) )
	{ }

	Iterator begin() const { return first; }
	Iterator end() const { return last; }

private:
	Iterator first;
	Iterator last;
};


// Documentação em MakeRange.dox
template<typename Iterator>
Range<Iterator> makeRange( std::pair<Iterator, Iterator> pair )
{
	return Range<Iterator>( std::move(pair.first), std::move(pair.second) );
}


}  // namespace util


#endif  // UTIL_MAKERANGE_HPP
